#include "aoc.hpp"

#include <fstream>
#include <string_view>
#include <ranges>
#include <algorithm>

size_t task_1(const std::string_view file);
size_t task_2(const std::string_view file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::string file{};
   std::string line{};
   while (std::getline(file_s, line)) {
      file += line + "\n";
   }
   file.erase(file.size()-1);


   Timer t;
   size_t task_1_ret = task_1(std::string_view{file});
   size_t task_2_ret = task_2(std::string_view{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


size_t task_1(const std::string_view file) {
   size_t sum = 0;
   for (const auto& line : std::views::split(file, ',')) {
      uint8_t cur_val = 0;
      for (uint i = 0; i < line.size(); i++) {
         cur_val += line[i];
         cur_val *= 17;
      }
      sum += cur_val;
   }
   return sum;
}

size_t task_2(const std::string_view file) {
   std::array<std::vector<std::pair<std::string_view, uint8_t>>, 256> boxes{};

   for (const auto& line : std::views::split(file, ',')) {
      uint8_t box = 0;
      uint i = 0;
      for (; isalpha(line[i]); i++) {
         box += line[i];
         box *= 17;
      }
      std::string_view label{line.begin(), line.begin() + i};

      std::vector<std::pair<std::string_view, uint8_t>>& cur_box = boxes[box];
      if (line[i] == '-') {
         //remove
         
         auto it = std::find_if(cur_box.begin(), cur_box.end(), [&](auto& e) {
            return e.first == label;
         });
         if (it != cur_box.end()) {
            cur_box.erase(it);
         }
      } else /* is '=' */ {
         i++;
         uint8_t nb = line[i] - '0';
         auto it = std::find_if(cur_box.begin(), cur_box.end(), [&](auto& e) {
            return e.first == label;
         });
         if (it != cur_box.end()) {
            it->second = nb;
         } else {
            cur_box.push_back({label, nb});
         }
         
      }
   }


   size_t sum = 0;
   for (uint j = 0; j < boxes.size(); j++) {
      const auto& box = boxes[j];
      for (uint i = 0; i < box.size(); i++) {
         const auto& e = box[i];
         size_t add = (j+1) * (i + 1) * e.second;
         sum += add;
      }
   }
   return sum;
}
