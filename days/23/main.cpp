#include "aoc.hpp"

#include <fstream>
#include <unordered_set>
#include <set>
#include <deque>
#include <map>
#include <functional>
#include <algorithm>
/* #include <limits> */

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


using Coord = std::pair<int16_t, int16_t>;

namespace std {
template<>
struct hash<Coord> {
   size_t operator()(const Coord& in) const {
      return (uint16_t(in.first) << 16) & uint16_t(in.second);
   }
};
}

bool operator<(const Coord& a, const Coord& b) {
   if (a.first == b.first) {
      return a.second < b.second;
   } else {
      return a.first < b.first;
   }
}

bool operator==(const Coord& a, const Coord& b) {
   return a.first == b.first && a.second == b.second;
}

Coord operator+(const Coord& a, const Coord& b) {
   return Coord{a.first + b.first, a.second + b.second};
}

/* std::ostream& operator<<(std::ostream& os, const Coord& a) { */
/*    /1* return os << "(" << a.first << ":" << a.second << ")"; *1/ */
/*    return os << "(" << a.second + 1 << ":" << a.first + 1 << ")"; */
/* } */

class Cell {
public:
   enum class Type {
      Tree,
      Trail,
      TrailUp,
      TrailRight,
      TrailDown,
      TrailLeft,
   };

   const Cell::Type type;
   bool visited{false};
   std::optional<Coord> visited_by{};
   size_t distance{0};

   Cell(const Cell::Type type) : type(type) {}

   void visit(std::optional<const Coord> in, std::optional<const Cell> in_cell) {
      visited = true;
      if (in) {
         visited_by = in;
         distance = in_cell->distance + 1;
      }
   }
   void visit() {
      visited = true;
   }
};


class Maze {
public:
   std::vector<std::vector<Cell>> cells{};

   Coord start{};
   Coord end{};

   Maze() = default;

   size_t Solve() {
      std::unordered_set<Coord> visited{};
      std::deque<Coord> to_visit{start};

      std::map<Coord, size_t> coord_to_pos{};
      std::map<size_t, Coord> pos_to_coord{};
      std::vector<std::vector<size_t>> adj_list{};

      while (!to_visit.empty()) {
         const Coord cur_coord = to_visit.front();
         to_visit.pop_front();
         const Cell& cur_cell = cells.at(cur_coord.second).at(cur_coord.first);

         /* std::cout << "cur_coord: " << cur_coord.first << " : " << cur_coord.second << "\n"; */

         /* if (visited.contains(cur_coord)) { */
         /*    std::cout << "skipping, already visisted\n"; */
         /*    continue; */
         /* } */

         //1) visit all elements with a queue, pushing the cells into a vector as vertices (uint),
         //  only connected with an edge to the neigbhour in a map

         if (!coord_to_pos.contains(cur_coord)) {
            coord_to_pos[cur_coord] = adj_list.size();
            pos_to_coord[adj_list.size()] = cur_coord;
            adj_list.emplace_back();
         }

         size_t cur_pos = coord_to_pos[cur_coord];

         //visit from previous
         /* cur_cell.visit(); */
         visited.insert(cur_coord);

         auto is_coord_connected_to_coord = [&](const Coord& a, const Coord& b) -> bool {
            if (!coord_to_pos.contains(a) || !coord_to_pos.contains(b)) { return false; }
            const auto& vec = adj_list[coord_to_pos[a]];
            return std::find(vec.begin(), vec.end(), coord_to_pos[b]) != vec.end();
         };
         auto check_neighbour = [&](const Coord dir) -> void {
            const Coord neigh_coord = cur_coord + dir;
            const Cell::Type neigh_type = cells.at(neigh_coord.second).at(neigh_coord.first).type;
            if (neigh_type != Cell::Type::Tree && !is_coord_connected_to_coord(neigh_coord, cur_coord)) {
               if (dir.first == -1 && !(neigh_type == Cell::Type::Trail || neigh_type == Cell::Type::TrailLeft)) return;
               else if (dir.first == 1 && !(neigh_type == Cell::Type::Trail || neigh_type == Cell::Type::TrailRight)) return;
               else if (dir.second == -1 && !(neigh_type == Cell::Type::Trail || neigh_type == Cell::Type::TrailUp)) return;
               else if (dir.second == 1 && !(neigh_type == Cell::Type::Trail || neigh_type == Cell::Type::TrailDown)) return;

               if (!coord_to_pos.contains(neigh_coord)) {
                  coord_to_pos[neigh_coord] = adj_list.size();
                  pos_to_coord[adj_list.size()] = neigh_coord;
                  adj_list.emplace_back();
               }
               adj_list.at(cur_pos).push_back(coord_to_pos[neigh_coord]);
               to_visit.push_back(neigh_coord);
            }
         };

         if (cur_coord.first > 0 && (cur_cell.type == Cell::Type::Trail || cur_cell.type == Cell::Type::TrailLeft))
            check_neighbour(Coord{-1, 0});
         if (cur_coord.first < int(cells.at(0).size()) - 1 && (cur_cell.type == Cell::Type::Trail || cur_cell.type == Cell::Type::TrailRight))
            check_neighbour(Coord{1, 0});
         if (cur_coord.second > 0 && (cur_cell.type == Cell::Type::Trail || cur_cell.type == Cell::Type::TrailUp))
            check_neighbour(Coord{0, -1});
         if (cur_coord.second < int(cells.size()) - 1 && (cur_cell.type == Cell::Type::Trail || cur_cell.type == Cell::Type::TrailDown))
            check_neighbour(Coord{0, 1});
      }

      //2) perform topological sorting
      /* std::cout << "nb_elems: " << adj_list.size() << "\n"; */

      std::vector<bool> visited_bool(adj_list.size(), false);
      std::vector<size_t> sorted_ind{};
      std::function<void(size_t)> topological;
      topological = [&](size_t v) {
         visited_bool[v] = true;

         for (auto& e : adj_list[v]) {
            if (!visited_bool[e]) {
               topological(e);
            }
         }

         sorted_ind.push_back(v);
      };

      for (uint i = 0; i < adj_list.size(); i++) {
         if (!visited_bool[i]) {
            topological(i);
         }
      }

      std::reverse(sorted_ind.begin(), sorted_ind.end());

      auto vert_ind_to_sorted_ind = [&](size_t v) -> size_t {
         return std::find(sorted_ind.begin(), sorted_ind.end(), v) - sorted_ind.begin();
      };

      //3) shortest path on negative vals
         
      std::vector<int32_t> dists(sorted_ind.size(), INT32_MAX);
      dists[0] = 0;
      for (uint i = 0; i < sorted_ind.size(); i++) {
         const auto& cur_v = sorted_ind[i];
         const auto& adj_vec = adj_list[cur_v];
         for (uint j = 0; j < adj_vec.size(); j++) {
            if (dists[vert_ind_to_sorted_ind(adj_vec[j])] > dists[i] - 1) {
               dists[vert_ind_to_sorted_ind(adj_vec[j])] = dists[i] - 1;
            }
         }
      }

      return -dists.back();
   }

   //Implement Bellman-Ford algorithm
   int32_t SolveNoSlopes(Coord cur_coord, /*std::set<Coord>&*/ std::vector<bool> visited, size_t cur_ret) {
      /* if (visited.contains(cur_coord)) { */
      /*    std::cerr<<"What.\n"; */
      /*    std::terminate(); */
      /* } */

      visited[cur_coord.second * cells.at(0).size() + cur_coord.first] = true;
      

      const auto get_neighbours = [&](Coord cur_coord) -> std::vector<Coord> {
         std::vector<Coord> unvisited_neighbours{};
         const auto check_neighbour = [&](const Coord dir) {
            const Coord neigh_coord = cur_coord + dir;
            if (!visited[neigh_coord.second * cells.at(0).size() + neigh_coord.first]) {
               unvisited_neighbours.push_back(neigh_coord);
            }
         };

         check_neighbour(Coord{-1, 0});
         check_neighbour(Coord{1, 0});
         if (cur_coord.second > 0) {
            check_neighbour(Coord{0, -1});
         }
         if (cur_coord.second < (int(cells.size()) - 1)) {
            check_neighbour(Coord{0, 1});
         }

         return unvisited_neighbours;
      };
      //check neighbours
      std::vector<Coord> unvisited_neighbours = get_neighbours(cur_coord);

      if (unvisited_neighbours.size() == 0) {
         /* std::cout << "cur_coord when no neighbours: " << cur_coord << "\n"; */
         if (cur_coord == end) {
            return cur_ret;
         } else {
            return -1'000'000;
         }
      } else if (unvisited_neighbours.size() == 1) {
         /* std::cout << "cur_coord when 1 neighbour: " << cur_coord << ", " << unvisited_neighbours[0] << "\n"; */

         while (unvisited_neighbours.size() == 1) {
            cur_coord = unvisited_neighbours[0];
            visited[cur_coord.second * cells.at(0).size() + cur_coord.first] = true;
            unvisited_neighbours = get_neighbours(cur_coord);
            cur_ret++;
         }

         return SolveNoSlopes(cur_coord, visited, cur_ret);
      } else {
         std::vector<int32_t> path_rets{};
         for (const auto& neigh : unvisited_neighbours) {
            std::vector<bool> new_visited = visited;
            int32_t ret = SolveNoSlopes(neigh, new_visited, cur_ret) + 1;
            path_rets.push_back(ret);
         }
         /* std::cout << "cur_coord when more than 1 neighbour: " << cur_coord << ", "; */
         /* for (uint i = 0; i < unvisited_neighbours.size(); i++) { */
         /*    std::cout << unvisited_neighbours[i] << " with value " << path_rets[i] << ", "; */
         /* } */
         /* std::cout << "\n"; */
         return *std::max_element(path_rets.begin(), path_rets.end());
      }
   }
};


size_t task_1(std::stringstream file) {
   std::string line;

   Maze maze{};
   for (uint j = 0; std::getline(file, line); j++) {
      std::vector<Cell> cur_cell{};
   
      for (uint i = 0; i < line.size(); i++) {
         switch (line.at(i)) {
            case '#': cur_cell.push_back(Cell{Cell::Type::Tree}); break;
            case '.': {
               if (j == 0) maze.start = Coord{i, 0};
               else if (j == line.size()-1) maze.end = Coord{i, j};
               cur_cell.push_back(Cell{Cell::Type::Trail});
            } break;
            case '^': cur_cell.push_back(Cell{Cell::Type::TrailUp}); break;
            case '>': cur_cell.push_back(Cell{Cell::Type::TrailRight}); break;
            case 'v': cur_cell.push_back(Cell{Cell::Type::TrailDown}); break;
            case '<': cur_cell.push_back(Cell{Cell::Type::TrailLeft}); break;
            default: __builtin_unreachable();
         }
      }

      maze.cells.push_back(std::move(cur_cell));
   }

   return maze.Solve();
}

size_t task_2(std::stringstream file) {
   std::string line;
   /* size_t sum = 0; */

   //TODO to solve this problem, what we could do is advancing one step at a time,
   // branching when there's 2 neighbours, keep a visited list for every branch
   // That would be fairly easy to implement with a recursive function. It would
   // probably be very slow, but longest path problems are pretty fundamentally slow
   // computationally, unless there's a hack I don't see (it's not a DAG anymore,
   // but maybe I could generate all the possible DAGs (( all of the possible turns )) and solve
   // those?)

   Maze maze{};
   for (uint j = 0; std::getline(file, line); j++) {
      std::vector<Cell> cur_cell{};
   
      for (uint i = 0; i < line.size(); i++) {
         switch (line.at(i)) {
            case '#': cur_cell.push_back(Cell{Cell::Type::Tree}); break;
            case '.': {
               if (j == 0) maze.start = Coord{i, 0};
               else if (j == line.size()-1) maze.end = Coord{i, j};
               cur_cell.push_back(Cell{Cell::Type::Trail});
            } break;
            case '^': cur_cell.push_back(Cell{Cell::Type::TrailUp}); break;
            case '>': cur_cell.push_back(Cell{Cell::Type::TrailRight}); break;
            case 'v': cur_cell.push_back(Cell{Cell::Type::TrailDown}); break;
            case '<': cur_cell.push_back(Cell{Cell::Type::TrailLeft}); break;
            default: __builtin_unreachable();
         }
      }

      maze.cells.push_back(std::move(cur_cell));
   }

   //TODO: ideas:
   //I think I can just implement Bellman-Ford's shortest path algorithm

   //TODO: ideas to speedup:
   //TODO: each branch that chooses a direction can gray out the path
   //TODO: reduce graph to just the junctions
   std::vector<bool> visited(maze.cells.size() * maze.cells.at(0).size(), false);
   for (uint i = 0; i < maze.cells.size(); i++) {
      for (uint j = 0; j < maze.cells.at(0).size(); j++) {
         if (maze.cells[i][j].type == Cell::Type::Tree) {
            visited[i * maze.cells.at(0).size() + j] = true;
         }
      }
   }
   return maze.SolveNoSlopes(maze.start, visited, 0);
}
