#include "aoc.hpp"

#include <fstream>
#include <ranges>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <queue>

size_t task_1(std::string_view file);
size_t task_2(std::string_view file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   std::string file(ss.str());
   if (!file.empty()) {
      file.erase(file.size()-1);
   }

   Timer t;
   size_t task_1_ret = task_1(std::string_view{file});
   size_t task_2_ret = task_2(std::string_view{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

namespace std {
template<>
struct hash<std::pair<std::string, std::string>> {
   size_t operator()(const pair<string, string>& in) const {
      return hash<string>{}(in.first) ^ hash<string>{}(in.second) << 1;
   }
};
}

class FlowEdge {
public:
   std::string from;
   std::string to;
   bool has_flow {false};
   //capacity is 1, so either it has 0 flow, or it has 1 flow

   FlowEdge() : from(""), to(""), has_flow(false) {}
   FlowEdge(const std::string from, const std::string to)
      : from(from), to(to), has_flow(false) {}

   std::string other(const std::string a) const {
      if (a == from) { return to; }
      else if (a == to) { return from; }
      else { die("Nope"); }
   }

   int residual_cap_to(const std::string a) const {
      if (a == from) {
         return has_flow ? 1 : 0;
      } else if (a == to) {
         return has_flow ? 0 : 1;
      } else { die("ah...\n"); }
   }

   void add_residual_flow_to(const std::string a) {
      if (a == from) {
         has_flow = false;
      } else if (a == to) {
         has_flow = true;
      } else { die(">///<\n"); }
   }

   bool operator==(const FlowEdge& a) const = default;
   friend std::ostream& operator<<(std::ostream& os, const FlowEdge& in) {
      return os << in.from << "->" << in.to << ", flow: " << in.has_flow;
   }
};

namespace std {
template<>
struct hash<FlowEdge> {
   size_t operator()(const FlowEdge& in) const {
      return hash<string>{}(in.from) ^ hash<string>{}(in.to) << 1 ^ in.has_flow;
   }
};
}

class FlowGraph {
public:
   std::unordered_map<std::pair<std::string, std::string>, FlowEdge> edges;
   std::unordered_map<std::string, std::vector<std::reference_wrapper<FlowEdge>>> neighs;

//Cache for last path
   std::unordered_set<std::string> visited;
   std::unordered_map<std::string, std::reference_wrapper<FlowEdge>> edge_to;

   void add_connection(const std::string a, const std::string b) {
      const FlowEdge f_a = {a, b};
      const FlowEdge f_b = {b, a};

      edges[{a, b}] = f_a;
      edges[{b, a}] = f_b;

      neighs[a].push_back(edges.at({a, b}));
      neighs[b].push_back(edges.at({b, a}));
   }

   //Calculates the path in visited/edge_to
   bool has_augmenting_path(const std::string a, const std::string b) {
      visited.clear();
      edge_to.clear();

      /* std::cout << "Starting augmenting path\n"; */

      std::deque<std::string> queue;
      queue.push_back(a);
      visited.insert(a);

      while (!queue.empty()) {
         const std::string cur_label = queue.front();
         queue.pop_front();
         
         for (FlowEdge& e : neighs.at(cur_label)) {
            const std::string w = e.other(cur_label);
            FlowEdge& rev_e = edges.at({e.to, e.from});

            if (e.residual_cap_to(w) > 0 && !visited.contains(w)) {
               edge_to.insert({w, e});
               visited.insert(w);
               queue.push_back(w);
            }
         }
      }

      /* std::cout << "visited:\n"; */
      /* for (const auto& e : visited) { */
      /*    std::cout << "- " << e << " "; */
      /* } */
      /* std::cout << "\n"; */

      /* std::cout << "edge_to:\n"; */
      /* for (const auto& [ to, edge ] : edge_to) { */
      /*    std::cout << "to: " << to << ", edge: " << edge << "\n"; */
      /* } */
      /* std::cout << "\n"; */

      /* std::cout << "Ending augmenting path\n\n"; */
      return visited.contains(b);
   }

   void solve_ford_fulkerson(const std::string a, const std::string b) {
      if (is_reused) {
         for (auto& [ _, edge ] : edges) { edge.has_flow = false; }
      }
      is_reused = true;
      visited.clear();
      edge_to.clear();

      while (has_augmenting_path(a, b)) {
         for (std::string cur_label = b; cur_label != a; cur_label = edge_to.at(cur_label).get().other(cur_label)) {
            FlowEdge& cur_edge = edge_to.at(cur_label);
            FlowEdge& rev_edge = edges.at({cur_edge.to, cur_edge.from});

            cur_edge.add_residual_flow_to(cur_label);
         }
      }
   }
private:
   bool is_reused { false };
};

size_t task_1(std::string_view file) {
   std::unordered_set<std::string> vertices{};

   FlowGraph flow_graph{};

   for (const auto& line_sp : std::views::split(file, '\n')) {
      const std::string_view line{line_sp};

      const std::string_view cur_label {line.begin(), line.begin() + 3};

      for (const auto& label_ : std::views::split(std::string_view{line.cbegin() + 5, line.cend()}, ' ')) {
         const std::string_view label{label_};
         vertices.insert(std::string{cur_label});
         vertices.insert(std::string{label});
         flow_graph.add_connection(std::string{cur_label}, std::string{label});
      }
   }

   const std::string start = *vertices.cbegin();
   for (const std::string& end : vertices) {
      if (end == start) { continue; }

      flow_graph.solve_ford_fulkerson(start, end);

      if (flow_graph.visited.size() != 1) { break; }
   }

   const size_t nb_vertices = flow_graph.visited.size();

   // Idea:
   // - find two vertices at random (loop until we find two in the different components, technical worst-case scenario is O(V/2), but usually it'll land on the right vertices on the 1st or 2nde try)
   // - solve the maximum flow problem/minimum cut, until we get a max flow of 3, and get those edges

   return nb_vertices * (vertices.size() - nb_vertices);
}

size_t task_2(std::string_view file) {
   for (const auto& line_sp : std::views::split(file, '\n')) {
      const std::string_view line{line_sp};
   }

   size_t sum = 0;
   return sum;
}
