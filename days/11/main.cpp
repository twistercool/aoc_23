#include "aoc.hpp"

#include <fstream>
#include <numeric>
#include <string_view>
#include <ranges>

size_t task_1(const std::string_view file);
size_t task_2(const std::string_view file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());
   
   const std::string_view sv{file};

   Timer t;
   size_t task_1_ret = task_1(sv);
   size_t task_2_ret = task_2(sv);
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


using Coord = std::pair<int32_t, int32_t>;

inline size_t calculate(const std::string_view file, const size_t increment) {
   const std::string __wut; //for some reason, having this string here improves the runtime??

   /* Timer t{"Task"}; */

   std::vector<Coord> galaxies{};
   galaxies.reserve(500);
   std::vector<bool> is_col_empty{};
   is_col_empty.reserve(140);
   std::vector<bool> is_row_empty{};
   is_row_empty.reserve(140);

   uint i = 0;
   for (const auto& line : std::views::split(file, '\n')) {
      if (line.size() == 0) { break; }
      if (is_col_empty.size() == 0) {
         is_col_empty.reserve(line.size());
         for (uint a = 0; a < line.size(); a++)
            is_col_empty.push_back(true);
      }
      bool row_empty = true;
      for (uint j = 0; j < line.size(); j++) {
         if (line[j] == '#') {
            is_col_empty[j] = false;
            row_empty = false;
            galaxies.emplace_back(j, i);
         }
      }
      is_row_empty.push_back(row_empty);
      i++;
   }

   /* t.PrintTimeMicrosReset(); */

   std::vector<size_t> col_nb_empties{};
   col_nb_empties.reserve(is_col_empty.size());
   std::accumulate(is_col_empty.cbegin(), is_col_empty.cend(), size_t(0LL), [&](size_t acc, bool cur) {
      if (cur) acc += increment;
      col_nb_empties.push_back(acc);
      return acc;
   });
   std::vector<size_t> row_nb_empties{};
   row_nb_empties.reserve(is_row_empty.size());
   std::accumulate(is_row_empty.cbegin(), is_row_empty.cend(), size_t(0LL), [&](size_t acc, bool cur) {
      if (cur) acc += increment;
      row_nb_empties.push_back(acc);
      return acc;
   });

   for (auto& co : galaxies) {
      co.first += col_nb_empties[co.first];
      co.second += row_nb_empties[co.second];
   }

   /* t.PrintTimeMicrosReset(); */


   /* __builtin_prefetch(&galaxies[0], 0, 3); */

   size_t sum = 0;
   const size_t g_size = galaxies.size();
   for (uint i = 0; i < g_size; i++) {
      const Coord a = galaxies[i];
      for (uint j = i + 1; j < g_size; j++) {
         const Coord b = galaxies[j];
         sum += std::abs(a.second - b.second) + std::abs(a.first - b.first);
      }
   }

   return sum;
}

inline size_t task_1(const std::string_view file) {
   return calculate(file, 1);
}

inline size_t task_2(const std::string_view file) {
   return calculate(file, 999'999);
}
