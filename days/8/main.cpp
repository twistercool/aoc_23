#include "aoc.hpp"

#include <fstream>
#include <algorithm>
#include <numeric>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

inline uint32_t to_hash(uint32_t in) {
   const uint32_t a = (in & 0xFF) - 'A';
   const uint32_t b = ((in & 0xFF00) >> 8) - 'A';
   const uint32_t c = ((in & 0xFF0000) >> 16) - 'A';
   const uint32_t ret = a * 26 * 26 + b * 26 + c;
   return ret;
};

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout
             << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

enum class Step {
   Left,
   Right
};


size_t task_1(std::stringstream file) {
   std::string line;
   
   if (!std::getline(file, line)) { die("Not first line"); }
   std::vector<Step> steps{};

   for (uint i = 0; i < line.size(); i++) {
      switch (line[i]) {
         case 'L': steps.push_back(Step::Left);  break;
         case 'R': steps.push_back(Step::Right); break;
         default: die("Incorrect input in first line");
      }
   }

   if (!std::getline(file, line)) { die("No second line"); }

   using Node = uint32_t;

   std::array<std::pair<Node, Node>, 25'000> nodes = {};
   for (uint i = 0; std::getline(file, line); i++) {
      const Node start = (line[0] << 16) + (line[1] << 8) + (line[2]);
      const Node left = (line[7] << 16) + (line[8] << 8) + (line[9]);
      const Node right = (line[12] << 16) + (line[13] << 8) + (line[14]);
      nodes[to_hash(start)] = {left, right};
   }


   Node cur_node = ('A' << 16) + ('A' << 8) + 'A';
   uint cur_step_ind = 0;
   uint sum = 0;
   while (true) {
      if (cur_node == (('Z' << 16) + ('Z' << 8) + 'Z')) {
         return sum;
      }
      if (steps.at(cur_step_ind) == Step::Left) {
         cur_node = nodes[to_hash(cur_node)].first;
      } else {
         cur_node = nodes[to_hash(cur_node)].second;
      }

      if (cur_step_ind >= steps.size() - 1) {
         cur_step_ind = 0;
      } else {
         cur_step_ind++;
      }
      sum++;
   }
}

size_t task_2(std::stringstream file) {
   std::string line;
   
   if (!std::getline(file, line)) { die("Not first line"); }
   std::vector<Step> steps{};

   for (uint i = 0; i < line.size(); i++) {
      switch (line[i]) {
         case 'L': steps.push_back(Step::Left);  break;
         case 'R': steps.push_back(Step::Right); break;
         default: die("Incorrect input in first line");
      }
   }

   if (!std::getline(file, line)) { die("No second line"); }

   using Node = uint32_t;

   std::array<std::pair<Node, Node>, 25'000> nodes = {};
   std::vector<Node> cur_nodes;
   for (uint i = 0; std::getline(file, line); i++) {
      const Node start = (line[0] << 16) + (line[1] << 8) + (line[2]);
      if (line[2] == 'A') {
         cur_nodes.push_back(start);
      }
      const Node left = (line[7] << 16) + (line[8] << 8) + (line[9]);
      const Node right = (line[12] << 16) + (line[13] << 8) + (line[14]);
      nodes[to_hash(start)] = {left, right};
   }

   std::vector<int64_t> cur_node_time_to_z{};
   for (uint i = 0; i < cur_nodes.size(); i++) {
      cur_node_time_to_z.push_back(-1);
   }

   uint cur_step_ind = 0;
   size_t sum = 0;
   while (true) {
      const bool any_remaining = std::any_of(cur_node_time_to_z.cbegin(), cur_node_time_to_z.cend(), [](const int in){
         return in == -1;
      });
      if (!any_remaining) {
         break;
      }

      size_t cnt = 0;
      for (Node& node: cur_nodes) {
         if ((node & 0xFF) == 'Z') {
            cur_node_time_to_z[cnt] = sum;
         }
         if (steps[cur_step_ind] == Step::Left) {
            node = nodes[to_hash(node)].first;
         } else {
            node = nodes[to_hash(node)].second;
         }
         cnt++;
      }

      if (cur_step_ind >= steps.size() - 1) {
         cur_step_ind = 0;
      } else {
         cur_step_ind++;
      }
      sum++;
   }

   const auto arr_lcm = [](const std::vector<int64_t>& in) -> uint64_t {
      uint64_t cur_lcm = 1;
      for (uint i = 0; i < in.size(); i++) {
         cur_lcm = std::lcm<uint64_t, uint64_t>(cur_lcm, in[i]);
      }
      return cur_lcm;
   };

   cur_node_time_to_z.push_back(steps.size());

   return arr_lcm(cur_node_time_to_z);
}
