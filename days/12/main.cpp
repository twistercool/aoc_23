#include "aoc.hpp"

#include <fstream>
/* #include <bitset> */
#include <map>
#include <numeric>
#include <unordered_map>
#include <map>
#include <cstring>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


bool is_valid_combination(const std::string_view in, const std::vector<size_t> damaged_lens) {
   size_t nb_len = 0;
   size_t index_vector = 0;
   for (uint i = 0; i < in.size(); ++i) {
      if (in.at(i) == '#') {
         nb_len++;
      } else {
         if (nb_len > 0) {
            if (index_vector >= damaged_lens.size() || damaged_lens[index_vector] != nb_len)
               return false;
            nb_len = 0;
            index_vector++;
         }
      }
   }
   bool is_end_valid;
   if (nb_len != 0) {
      is_end_valid = (index_vector == damaged_lens.size() - 1) && (damaged_lens[index_vector] == nb_len);
   } else {
      is_end_valid = index_vector == damaged_lens.size();
   }
   return is_end_valid;
}

namespace std {
template<>
struct hash<std::pair<size_t, std::vector<size_t>>> {
   size_t operator()(const std::pair<size_t, std::vector<size_t>>& in) const {
      size_t ret = in.first;
      for (const auto& nb : in.second) {
         ret ^= nb;
         ret <<= 2;
      }
      return ret;
   }
};
}

//When there's a raw patch of unknows like '????????'
//and a vector of numbers, calculate
//Assume that the sv is fully '?'
size_t count_contiguous_unknows(const std::string_view sv, const std::vector<size_t> damaged) {
   static std::unordered_map<std::pair<size_t, std::vector<size_t>>, size_t> cached_response{};
   if (damaged.size() == 0) {
      return 0;
   } else if (damaged.size() == 1) {
      if (sv.size() < damaged.front()) {
         return 0;
      } else {
         return sv.size() - damaged.front() + 1;
      }
   }

   if (cached_response.contains({sv.size(), damaged})) {
      return cached_response.at({sv.size(), damaged});
   }

   size_t sum_damaged = std::accumulate(damaged.cbegin(), damaged.cend(), int64_t{-1}, [](int64_t acc, size_t cur) { return acc + cur + 1; });

   if (sum_damaged == sv.size()) return 1;
   else if (sum_damaged > sv.size()) return 0;

   size_t ret = 0;
   for (uint i = 0; i < sv.size() - damaged.front() + 1; i++) {
      size_t cur = count_contiguous_unknows(std::string_view{sv.begin() + i + damaged.front() + 1, sv.end()}, std::vector<size_t>(damaged.begin() + 1, damaged.end()));
      if (cur == 0) {
         break;
      } else {
         ret += cur;
      }
   }

   cached_response.insert_or_assign({sv.size(), damaged}, ret);

   return ret;
}


class Spring {
public:
   std::string parts;
   std::vector<size_t> damaged;
   size_t total_damaged = 0;
   size_t seen_damaged = 0;
   size_t nb_unknown = 0;

   Spring() = default;
   Spring(const Spring& in) = default;
   Spring& operator=(const Spring& in) = default;
   bool operator==(const Spring& b) const = default;

   friend int operator<=>(const Spring& a, const Spring& b) {
      if (a.parts != b.parts) {
         return std::strcmp(a.parts.c_str(), b.parts.c_str());
      } else if (a.total_damaged != b.total_damaged) {
         return int(a.total_damaged) - int(b.total_damaged);
      } else if (a.damaged.size() != b.damaged.size()) {
         return int(a.damaged.size()) - int(b.damaged.size());
      }

      for (uint i = 0; i < a.damaged.size(); i++) {
         if (a.damaged.at(i) != b.damaged.at(i)) {
            return int(a.damaged.at(i)) - int(b.damaged.at(i));
         }
      }

      return 0;
   };

   friend std::ostream& operator<<(std::ostream& os, const Spring& in) {
      os << "Spring: '" << in.parts << "' with parts: '";
      for (const auto& e : in.damaged) {
         os << " " << e;
      }
      return os << " ', total_damaged: '" << in.total_damaged << "', seend damaged: '" << in.seen_damaged << "', nb_unknow: '" << in.nb_unknown << "'\n";
   }

   bool is_impossible_combination() const {
      if (seen_damaged > total_damaged) {
         return true;
      } else if (total_damaged > parts.size()) {
         return true;
      }

      return false;
   }

   //Remove unecessary '.'s
   void CleanDots() {
      size_t leading_dots = 0;
      for (uint i = 0; i < parts.size() && parts.at(i) == '.'; i++) {
         leading_dots++;
      }
      if (leading_dots > 0) {
         parts.erase(0, leading_dots);
      }
      size_t trailing_dots = 0;
      for (int i = parts.size()-1; i >= 0 && parts.at(i) == '.'; i--) {
         trailing_dots++;
      }
      if (trailing_dots > 0) {
         parts.erase(parts.size() - trailing_dots, trailing_dots);
      }

      for (uint i = 0; i < parts.size(); i++) {
         size_t nb_dots = 0;
         if (parts[i] == '.') {
            while (i + nb_dots < parts.size() && parts[i + nb_dots] == '.') { nb_dots++; }
            if (nb_dots > 1) {
               parts.erase(i + 1, nb_dots - 1);
               continue;
            }
         }
      }
   }

   size_t CalculateArragementsRec();
};

namespace std {
template<>
struct std::hash<Spring> {
   size_t operator()(const Spring& in) const {
      return std::hash<std::string>{}(in.parts) ^ in.nb_unknown;
   }
};
}

size_t Spring::CalculateArragementsRec() {
   CleanDots();

   static std::unordered_map<Spring, size_t> cache{};
   if (cache.contains(*this)) {
      return cache[*this];
   }

   if (is_impossible_combination()) {
      return 0;
   }
   if (damaged.size() == 0 || total_damaged == seen_damaged) {
      for (auto& part : parts) {
         if (part == '?') {
            nb_unknown--;
            part = '.';
         }
      }
      return is_valid_combination(parts, damaged) ? 1 : 0;
   }
   if (nb_unknown == 0) {
      return is_valid_combination(parts, damaged) ? 1 : 0;
   }

   //After this, the state of the Spring should be normal (parts.size() > 0, damaged.size() > 0, nb_unknows > 0)

   //if all of the parts are unknows and continguous, can calculate it very quickly
   if (parts.front() == '?') {
      bool is_all_unknowns = true;
      uint ind = 0;
      for (; ind < parts.size(); ind++) {
         if (parts.at(ind) != '?') {
            is_all_unknowns = false;
            break;
         }
      }
      if (is_all_unknowns) {
         return count_contiguous_unknows(parts, damaged);
      }
   }

   //If the input starts/ends with a '#' and has damaged parts, we can assume that all of the '#' and '?' in a radius are part of the same part (and remove them)
   if (parts.front() == '#') {
      size_t expected_len = damaged.front();
      if (parts.size() < expected_len) {
         return 0;
      }
      uint i = 0;
      for (; i < expected_len; i++) {
         const char c = parts[i];
         if (c == '.') {
            return 0; //invalid
         } else if (c == '?') {
            parts[i] = '#';
            nb_unknown--;
            seen_damaged++;
          } //else it's a '#'
      }
      if (i < parts.size() && parts[i] == '#') {
         return 0;
      } else if (i < parts.size() && parts[i] == '?') {
         parts[i] = '.';
         nb_unknown--;
      }
      while (parts.size() > 0 && parts.front() == '#') {
         parts.erase(parts.begin());
         seen_damaged--;
         total_damaged--;
      }
      damaged.erase(damaged.begin());

      return CalculateArragementsRec();
   }
   if (parts.back() == '#') {
      size_t expected_len = damaged.back();
      if (parts.size() < expected_len) {
         return 0;
      }
      int i = parts.size()-1;
      for (; i >= int(parts.size() - expected_len); i--) {
         const char c = parts[i];
         if (c == '.') {
            return 0; //invalid
         } else if (c == '?') {
            parts[i] = '#';
            nb_unknown--;
            seen_damaged++;
          } //else it's a '#'
      }
      if (uint(i) < parts.size() && parts[i] == '#') {
         return 0;
      } else if (uint(i) < parts.size() && parts[i] == '?') {
         parts[i] = '.';
         nb_unknown--;
      }
      while (parts.size() > 0 && parts.back() == '#') {
         parts.erase(parts.end()-1);
         seen_damaged--;
         total_damaged--;
      }
      damaged.erase(damaged.end()-1);
      return CalculateArragementsRec();
   }


   //check first patch, if it's not possible to put the first 2 elements, then the first element is necessarily in the first patch (or it's wrong)
   if (parts.front() == '?') {
      size_t size_patch = 0;
      uint ind = 0;
      bool patch_pure_unknow = true;
      for (; ind < parts.size() && (parts[ind] == '?' || parts[ind] == '#') ; ind++) {
         if (parts[ind] == '#') {
            patch_pure_unknow = false;
         }
         size_patch++;
      }

      if (patch_pure_unknow && size_patch < damaged.at(0)) {
         nb_unknown -= ind;
         parts = parts.substr(ind);
         return CalculateArragementsRec();
      }

      //First patch guaranteed to only hold at most one damaged_spring
      if (damaged.size() >= 2 && damaged.at(0) + 1 + damaged.at(1) > ind) {
         //Pure patch might hold 0 or 1 damaged
         if (patch_pure_unknow) {
            nb_unknown -= ind;

            Spring a{*this};
            a.parts = a.parts.substr(ind + 1);

            total_damaged -= damaged.at(0);
            size_t nb_possibilities_in_patch = count_contiguous_unknows(parts.substr(0, ind), std::vector<size_t>{damaged.at(0)});
            parts = parts.substr(ind + 1);
            damaged.erase(damaged.begin());


            size_t ret = nb_possibilities_in_patch * CalculateArragementsRec() + a.CalculateArragementsRec();
            /* cache.insert_or_assign(*this, ret); */
            return ret;
         } else {
            //TODO several strings can be put there, try to calculate in a smart way
         }
      }
   }

   size_t first_damaged = parts.find('#');

   //TODO: fix it, it's likely wrong
   //guarantees that the first spring is at the location of the first seen damage
   if (first_damaged != std::string::npos && first_damaged < damaged.at(0) + 1) {
      size_t len_damaged = 0;
      for (uint i = first_damaged; i < parts.size() && parts.at(i) == '#'; i++)
         len_damaged++;

      if (len_damaged > damaged.front()) {
         return 0;
      } else if (len_damaged == damaged.front()) {
         for (uint i = 0; i < first_damaged; i++) {
            if (parts.at(i) == '?') {
               nb_unknown--;
               parts.at(i) = '.';
            }
         }
         for (uint i = first_damaged; i < first_damaged + len_damaged; i++) {
            parts.at(i) = '.';
            seen_damaged--;
         }
         size_t index_unknow_after = first_damaged + len_damaged;
         if (index_unknow_after < parts.size() && parts.at(index_unknow_after) == '?') {
            nb_unknown--;
            parts.at(index_unknow_after) = '.';
         }
         total_damaged -= damaged.front();
         damaged.erase(damaged.begin());
         return CalculateArragementsRec();
      }

      //TODO else the spring is still 100% at the location of the first damaged, but it could be different locations (which potentially affect the following springs, so it needs recalculating)

      //TODO: check if there are springs afterwards that force something
      /* size_t first_unknow = parts.find('?'); */
      /* size_t len_unknows_before = 0; */
      /* for (int i = first_damaged - 1; i >= 0 && parts.at(i) == '?'; i--) */
      /*    len_unknows_before++; */
      /* size_t len_unknows_after = 0; */
      /* for (uint i = first_damaged + len_damaged + 1; i < parts.size() && parts.at(i) == '?'; i++) */
      /*    len_unknows_after++; */
   }


   //No shortcut found, needs to branch on the first unknow
   Spring a{*this};

   //Creates new entry, and assign to it later
   size_t& cache_ret = cache[*this];

   size_t sum = 0;
   //counts the valid states if the first unknown is '.'
   uint i = 0;
   for (; i < a.parts.size(); i++) {
      if (a.parts[i] == '?') {
         a.parts[i] = '.';
         a.nb_unknown--;
         sum += a.CalculateArragementsRec();
         break;
      }
   }

   //Note: reusing the same spring
   //Counts the valid states if the first unknown is a '#'
   //TODO: try to insert the entire damaged spring here
   parts.at(i) = '#';
   seen_damaged++;
   nb_unknown--;

   sum += CalculateArragementsRec();
   if (--damaged[0] == 0) {
      damaged.erase(damaged.begin());
   }

   cache_ret = sum;

   return sum;
}


size_t task_1(std::stringstream file) {
   std::string line;
   size_t sum = 0;

   for (uint i = 0; std::getline(file, line); i++) {
      Spring spring;
      size_t ind = 0;
      for (; ind < line.size(); ind++/*, spring.parts_uint <<= 1*/) {
         char c = line[ind];
         if (c == ' ') break;
         else if (c == '#') {
            spring.seen_damaged++;
         } else if (c == '?') {
            spring.nb_unknown++;
         }
      }
      spring.parts = line.substr(0, ind);
      ind++;

      size_t nb = 0;
      for (; ind < line.size(); ++ind) {
         char c = line[ind];
         if (isdigit(c)) {
            nb = nb * 10 + c - '0';
            continue;
         } else if (c == ',') {
            spring.damaged.push_back(nb);
            spring.total_damaged += nb;
            nb = 0;
         } else { die("UHHHHHHHHHHHHHHHHH"); }
      }
      spring.damaged.push_back(nb);
      spring.total_damaged += nb;

      size_t nb_arr = spring.CalculateArragementsRec();
      sum += nb_arr;
   }

   return sum;
}

size_t task_2(std::stringstream file) {
   std::string li;
   size_t sum = 0;

   for (uint i = 0; std::getline(file, li); i++) {
      size_t ind_space = li.find(' ');
      std::string line;
      for (uint j = 0; j < 4; j++)
         line += li.substr(0, ind_space) + '?';
      line += li.substr(0, ind_space) + ' ';
      for (uint j = 0; j < 4; j++)
         line += li.substr(ind_space+1) + ',';
      line.append(li.substr(ind_space+1));
   

      Spring spring;
      size_t ind = 0;
      for (; ind < line.size(); ind++) {
         char c = line[ind];
         if (c == ' ') break;
         else if (c == '#') spring.seen_damaged++;
         else if (c == '?') spring.nb_unknown++;
      }
      spring.parts = line.substr(0, ind);
      ind++;

      size_t nb = 0;
      for (; ind < line.size(); ++ind) {
         char c = line[ind];
         if (isdigit(c)) {
            nb = nb * 10 + c - '0';
         } else if (c == ',') {
            spring.damaged.push_back(nb);
            spring.total_damaged += nb;
            nb = 0;
         } else { die("UHHHHHHHHHHHHHHHHH"); }
      }
      spring.damaged.push_back(nb);
      spring.total_damaged += nb;

      sum += spring.CalculateArragementsRec();
   }

   return sum;
}
