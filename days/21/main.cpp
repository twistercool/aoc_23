#include "aoc.hpp"

#include <fstream>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <queue>
#include <ranges>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);
size_t task_2_(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2_(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout
             << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


size_t task_1(std::stringstream file) {
   std::string line;
   size_t sum = 0;

   Coordi32 start{};
   std::vector<std::vector<bool>> map{};
   for (uint i = 0; std::getline(file, line); i++) {
      map.emplace_back();
      for (uint j = 0; j < line.size(); j++) {
         if (line[j] == '.') {
            map.back().push_back(true);
         } else if (line[j] == '#') {
            map.back().push_back(false);
         } else /*it's an S*/ {
            map.back().push_back(true);
            start = {j, i};
         }
      }
   }


   std::unordered_set<Coordi32> prev{start};
   std::unordered_set<Coordi32> cur_iter{};

   const auto add_neighbours = [&](const Coordi32& in) -> void {
      for (const Diri32& dir : { Diri32{0, -1}, Diri32{0, 1}, Diri32{1, 0}, Diri32{-1, 0} }) {
         const Coordi32 neigh = in + dir;
         if (neigh.first >= 0 && neigh.first < int32_t(map.at(0).size()) && neigh.second >= 0 && neigh.second < int32_t(map.size()) && map[neigh.second][neigh.first]) {
            cur_iter.insert(neigh);
         }
      }
   };

   for (uint i = 0; i < 64; i++) {
      //for all prev coords, find the neighbours
      cur_iter = {};
      for (const Coordi32& coord : prev) {
         add_neighbours(coord);
      }
      prev = std::move(cur_iter);
   }

   return prev.size();
}

size_t task_2(std::stringstream file) {
   std::string line;

   Coordi32 start{};
   std::vector<std::vector<bool>> map{};
   for (uint i = 0; std::getline(file, line); i++) {
      map.emplace_back();
      for (uint j = 0; j < line.size(); j++) {
         if (line[j] == '.') {
            map.back().push_back(true);
         } else if (line[j] == '#') {
            map.back().push_back(false);
         } else /*it's an S*/ {
            map.back().push_back(true);
            start = {j, i};
         }
      }
   }

   const Coordi32 m_s{map.at(0).size(), map.size()};

   const auto is_coord_on_plot = [&](const Coordi32 in) -> bool {
      const size_t x = ((in.first % m_s.first) + m_s.first) % m_s.first;
      const size_t y = ((in.second % m_s.second) + m_s.second) % m_s.second;
      return map[y][x];
   };
   /* const uint max_i = 26501365; */
   const uint max_i = 5000;

   std::unordered_set<Coordi32> prev{start};
   std::unordered_set<Coordi32> cur_iter{};

   const auto dist_start = [&](const Coordi32& in) {
      /* const auto ret = std::abs(start.first - in.first) + std::abs(start.second - in.second); */
      /* std::cout << "Dist of: '" << in << "' to start'" << start << "' is: '" << ret << "'\n"; */
      return std::abs(start.first - in.first) + std::abs(start.second - in.second);
   };

   const auto add_neighbours = [&](const Coordi32& in) -> void {
      for (const Diri32& dir : { Diri32{0, -1}, {0, 1}, {1, 0}, {-1, 0} }) {
         const Coordi32 neigh = in + dir;
         if (is_coord_on_plot(neigh)) {
            cur_iter.insert(neigh);
         }
      }
   };

   /* for (int i = 0; i < int(max_i); i++) { */
   /*    //for all prev coords, find the neighbours */
   /*    cur_iter = {}; */
   /*    for (const Coordi32& coord : prev) { */
   /*       add_neighbours(coord); */
   /*    } */
   /*    prev = std::move(cur_iter); */
   /* } */

   /* std::cout << "original: " << prev.size() << '\n'; */

   /* prev = {start}; */
   /* cur_iter.clear(); */


   size_t nb_odd = 0;
   size_t nb_even = 0;

   size_t nb_cur_odd = 0;
   size_t nb_cur_even = 0;

   int64_t max_layer_cached = 0;

   //TODO: IDEA: get all elements below a certain layer for even and odd (so over 2 loops, let's say at mod 10==0 and mod 10==1)
   //Then remove all of those elements under the threshold and add them to a cache of even and odd
   //

   /* const auto garbage_collect = [&](const uint max_i) -> void { */
   /*    //TODO: get all the elements of under a given layer (for even and odd, so need to calculate next ) */
   /*    // */
   /* }; */

   const int64_t keep_layers = 100;
   for (int i = 0; i < int(max_i); i++) {
      //for all prev coords, find the neighbours
      cur_iter = {};
      for (const Coordi32& coord : prev) {
         add_neighbours(coord);
      }
      prev = std::move(cur_iter);

      if (i >= 30) {
         if (i % 10 == 0) {
            //Count the nb of elements at dists [i-20, i-10) (even)
            nb_cur_even = std::count_if(prev.cbegin(), prev.cend(), [&](const Coordi32& e) { const auto dist = dist_start(e); return dist >= i - 10 - keep_layers && dist < i - keep_layers; });
         } else if (i % 10 == 1) {
            //Count the nb of elements at dists [i-20, i-10) (odd)
            nb_cur_odd = std::count_if(prev.cbegin(), prev.cend(), [&](const Coordi32& e) { const auto dist = dist_start(e); return dist >= i - 11 - keep_layers && dist < i - 1 - keep_layers; });

            //then, remove all elements below i-20 because they're cached anyways
            std::erase_if(prev, [&](const Coordi32& e) { const auto dist = dist_start(e); return dist < i - 11 - keep_layers; });
            nb_odd += nb_cur_odd;
            nb_even += nb_cur_even;

            max_layer_cached = i - 1 - keep_layers;
            /* std::cout << "max_layer_cached: " << max_layer_cached << " nb_odd: " << nb_odd << " nb_even: " << nb_even << "\n"; */
         }
      }
   }

   /* size_t sum = 0; */
   /* for (const auto& e : prev) { */
      /* if (dist_start(e) >= max_layer_cached) { */
         /* std::cout << e << "\n"; */
         /* sum++; */
      /* } */
   /* } */
   /* std::cout << "sum: " << sum << '\n'; */


   //Remove elements that are cached
   std::erase_if(prev, [&](const Coordi32& e) { return dist_start(e) < max_layer_cached; });

   return prev.size() + ((max_i % 2 == 0) ? nb_odd : nb_even);
   /* return prev.size(); */
}

/*
 * Copied answer from HyperNeutrino, the answer is bullshit, hyper-specific to the input and
 * I'm not interested at all in finding hacks like these (yes I'm salty about my solution taking weeks)
*/
size_t task_2_(std::stringstream file) {
   std::string line;

   Coordi32 start{};
   std::vector<std::vector<bool>> map{};
   for (uint i = 0; std::getline(file, line); i++) {
      map.emplace_back();
      for (uint j = 0; j < line.size(); j++) {
         if (line[j] == '.') {
            map.back().push_back(true);
         } else if (line[j] == '#') {
            map.back().push_back(false);
         } else /*it's an S*/ {
            map.back().push_back(true);
            start = {j, i};
         }
      }
   }

   const Coordi32 m_s{map.at(0).size(), map.size()};
   const size_t m_size = m_s.first;

   const auto is_coord_on_plot = [&](const Coordi32 in) -> bool {
      const size_t x = positive_mod(in.first, m_s.first);
      const size_t y = positive_mod(in.second, m_s.second);
      return map[y][x];
   };

   const uint max_i = 26501365;

   const auto f = [&](const size_t max_i) -> size_t {
      std::unordered_set<Coordi32> prev{start};
      std::unordered_set<Coordi32> cur_iter{};

      const auto add_neighbours = [&](const Coordi32& in) -> void {
         for (const Diri32& dir : { Diri32{0, -1}, {0, 1}, {1, 0}, {-1, 0} }) {
            const Coordi32 neigh = in + dir;
            if (is_coord_on_plot(neigh)) {
               cur_iter.insert(neigh);
            }
         }
      };

      for (auto _ : std::views::iota(0, int(max_i))) {
         //for all prev coords, find the neighbours
         cur_iter = {};
         for (const Coordi32& coord : prev) {
            add_neighbours(coord);
         }
         prev = std::move(cur_iter);
      }

      return prev.size();
   };

   //

   auto x = max_i % (2 * m_s.first);
   std::deque<int64_t> vals{};

   while (true) {
      vals.push_back(f(x));
      x += 2 * m_size;

      if (vals.size() >= 4) {
         std::vector<int64_t> fd = {
            vals[1] - vals[0],
            vals[2] - vals[1],
            vals[3] - vals[2],
         };
         std::vector<int64_t> sd = {
            fd[1] - fd[0],
            fd[2] - fd[1],
         };
         if (sd[0] == sd[1]) {
            break;
         } else {
            vals.pop_front();
         }
      }
   }

   std::cout << vals;

   auto c = vals[0];
   auto a = (vals[2] - 2 * vals[1] + c) / 2;
   auto b = vals[1] - c - a;

   const auto f_ = [&](const auto x) -> size_t {
      return a * x * x + b * x + c;
   };

   return f_(max_i / (2 * m_size));
}
