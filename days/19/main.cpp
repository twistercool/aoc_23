#include "aoc.hpp"

#include <fstream>
#include <functional>
#include <string_view>
#include <ranges>
#include <cassert>
#include <unordered_map>

size_t task_1(std::string_view file);
size_t task_2(std::string_view file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   std::string file(ss.str());
   if (file.size() > 0)
      file.erase(file.end()-1);


   Timer t;
   size_t task_1_ret = task_1(std::string_view{file});
   size_t task_2_ret = task_2(std::string_view{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


class Part {
public:
   int32_t x = 0, m = 0, a = 0, s = 0;

   //Assumes coorect
   Part(const std::string_view in) {
      int which = 0;
      for (const auto category : std::views::split(std::string_view{in.begin() + 1, in.end() - 1}, ',')) {
         size_t nb = 0;
         for (uint i = 2; i < category.size(); i++)
            nb = nb * 10 + category[i] - '0';

         if (which == 0) x = nb;
         else if (which == 1) m = nb;
         else if (which == 2) a = nb;
         else if (which == 3) s = nb;

         which++;
      }
   }

   friend std::ostream& operator<<(std::ostream& os, const Part& a) {
      return os << "Part: (x: " << a.x << ", m: " << a.m << ", a: " << a.a << ", s: " << a.s << ")\n";
   }
};

class Workflow {
public:
   std::vector<std::function<bool(const Part&)>> predicates;
   std::vector<std::string> labels;
   
   std::string operator()(const Part& in) const {
      size_t ind = 0;
      for (const auto& predicate : predicates) {
         if (predicate(in)) {
            return labels.at(ind);
         }
         ind++;
      }
      assert(0);
   }

   friend std::ostream& operator<<(std::ostream& os, const Workflow& a) {
      for (uint i = 0; i < a.predicates.size(); i++) {
         os << "Predicate: (" << "...), label: " << /*a.predicates.at(i)*/ a.labels.at(i);
      }
      return os;
   }
};


size_t task_1(std::string_view file) {
   std::string line;
   size_t sum = 0;

   std::unordered_map<std::string, Workflow> workflows;
   std::vector<Part> parts{};

   bool parsing_workflows = true;
   for (const auto line : std::views::split(file, '\n')) {
      if (line.size() == 0) {
         parsing_workflows = false;
         continue;
      }

      if (parsing_workflows) {
         Workflow cur_workflow{};
         size_t ind_lcurly = std::find(line.begin(), line.end(), '{') - line.begin();
         assert(ind_lcurly < line.size());
         const std::string label{line.begin(), line.begin() + ind_lcurly};

         for (const auto cur_predicate
            : std::views::split(std::string_view{line.begin() + ind_lcurly + 1, line.end() - 1}, ','))
         {
            const std::string_view cur_predicate_sv{cur_predicate.begin(), cur_predicate.end()};

            uint i = 0;
            for (; i < cur_predicate_sv.size(); i++) { if (cur_predicate_sv.at(i) == ':') { break; } }

            if (i == cur_predicate.size()) {
               cur_workflow.predicates.push_back([](const Part& in) -> bool { return true; });
               cur_workflow.labels.push_back(std::string{cur_predicate.begin(), cur_predicate.end()});
            } else {
               const std::function<bool(int64_t, int64_t)> cmp_func = *(cur_predicate.begin() + 1) == '<'
                  ? std::function<bool(int64_t, int64_t)>(std::less<int64_t>())
                  : std::function<bool(int64_t, int64_t)>(std::greater<int64_t>());
               
               int64_t nb = 0;
               for (uint j = 2; j < i; j++) {
                  nb = 10 * nb + cur_predicate_sv.at(j) - '0';
               }

               std::function<bool(const Part&)> pred;
               switch (*cur_predicate_sv.begin()) {
                  case 'x': {
                     pred = [=](const Part& in) -> bool { return cmp_func(in.x, nb); };
                  } break;
                  case 'm': {
                     pred = [=](const Part& in) -> bool { return cmp_func(in.m, nb); };
                  } break;
                  case 'a': {
                     pred = [=](const Part& in) -> bool { return cmp_func(in.a, nb); };
                  } break;
                  case 's': {
                     pred = [=](const Part& in) -> bool { return cmp_func(in.s, nb); };
                  } break;
               }

               cur_workflow.predicates.push_back(pred);
               cur_workflow.labels.push_back(std::string{cur_predicate_sv.begin() + i + 1, cur_predicate.end()});
            }
         }

         workflows.insert_or_assign(label, cur_workflow);
      } else {
         parts.emplace_back(std::string_view{line.begin(), line.end()});
      }
   }

   for (Part& cur_part : parts) {
      std::string cur_label{"in"};
      while (cur_label != "R" && cur_label != "A") {
         cur_label = workflows.at(cur_label)(cur_part);
      }

      if (cur_label == "A") {
         sum += cur_part.x + cur_part.m + cur_part.a +cur_part.s;
      }
   }



   return sum;
}

class Rule {
public:
   enum class Type : uint8_t {
      Accept, // 'A' or 'R'
      Teleport, // like 'qrs'
      LogicTeleport, // like 'a<3000:qrs'
      LogicAccept, // like 'a<3000:qrs'
   };
   Type type;
   bool is_accept;
   std::string label;
   char which_char;
   size_t nb;
   bool is_less;

   Rule(const std::string_view sv) {
      if (sv == "A") {
         type = Type::Accept;
         is_accept = true;
      } else if (sv == "R") {
         type = Type::Accept;
         is_accept = false;
      } else {
         auto l = sv.find('<'), r = sv.find('>');
         if ((l & r) == std::string_view::npos) {
            type = Type::Teleport;
            label = std::string{sv};
         } else { //Is Type::Logic<...>
            is_less = (r == std::string_view::npos);
            auto ind = std::min(l, r);
            which_char = sv.at(0);
            const auto ind_colon = sv.find(':');
            const std::string_view nb_sv { sv.begin() + 2, sv.begin() + ind_colon };
            std::from_chars(nb_sv.data(), nb_sv.data() + nb_sv.size(), nb);

            const std::string_view post_label { sv.begin() + ind_colon + 1, sv.end() };
            if (post_label == "A") {
               type = Type::LogicAccept;
               is_accept = true;
            } else if (post_label == "R") {
               type = Type::LogicAccept;
               is_accept = false;
            } else {
               type = Type::LogicTeleport;
               label = std::string{post_label};
            }
         }
      }
   }

   friend std::ostream& operator<<(std::ostream& os, const Rule& in) {
      switch (in.type) {
         case Rule::Type::Accept: {
            return os << "Accept: " << (in.is_accept ? "A" : "R");
         } break;
         case Rule::Type::Teleport: {
            return os << "Teleport: " << in.label;
         } break;
         case Rule::Type::LogicAccept: {
            return os << "LogicAccept: " << in.which_char << (in.is_less ? " < " : " > ") << in.nb << " -> " << (in.is_accept ? "A" : "R");
         } break;
         case Rule::Type::LogicTeleport: {
            return os << "LogicTeleport: " << in.which_char << (in.is_less ? " < " : " > ") << in.nb << " -> " << in.label;
         } break;
      }
   }
};

class Range {
public:
//Half-open ranges: {0, 5} => 0, 1, 2, 3, 4
   std::pair<size_t, size_t> x{};
   std::pair<size_t, size_t> m{};
   std::pair<size_t, size_t> a{};
   std::pair<size_t, size_t> s{};

   Range(std::pair<size_t, size_t> x = {0, 0}, std::pair<size_t, size_t> m = {0, 0}, std::pair<size_t, size_t> a = {0, 0}, std::pair<size_t, size_t> s = {0, 0}) : x{x}, m{m}, a{a}, s{s} {

   }

   // {accept, reject}
   std::pair<Range, Range> split_range(const Rule rule) const {
      switch (rule.type) {
         case Rule::Type::Accept: {
            if (rule.is_accept) return {*this, {}};
            else return {{}, *this};
         }; break;
         case Rule::Type::Teleport: {
            return {*this, {}};
         }; break;
         case Rule::Type::LogicTeleport: /*fallthrough*/
         case Rule::Type::LogicAccept: {
            size_t cur_first;
            size_t cur_second;
            switch (rule.which_char) {
               case 'x': cur_first = x.first; cur_second = x.second; break;
               case 'm': cur_first = m.first; cur_second = m.second; break;
               case 'a': cur_first = a.first; cur_second = a.second; break;
               case 's': cur_first = s.first; cur_second = s.second; break;
            }

            if (rule.nb > cur_second) {
               if (rule.is_less) return { { x, m, a, s } , {} };
               else return { {}, { x, m, a, s } };
            } else if (rule.nb < cur_first) {
               if (!rule.is_less) return { { x, m, a, s } , {} };
               else return { {}, { x, m, a, s } };
            } else if (rule.nb == cur_second) {
               switch (rule.which_char) {
                  case 'x': {
                     if (rule.is_less) return { { {x.first, rule.nb}, m, a, s } , { {rule.nb, x.second + 1} , m, a, s } };
                     else return { {} , {x, m, a, s} };
                  }; break;
                  case 'm': {
                     if (rule.is_less) return { { x, {m.first, rule.nb}, a, s } , { x, {rule.nb, m.second + 1}, a, s } };
                     else return { {} , {x, m, a, s} };
                  }; break;
                  case 'a': {
                     if (rule.is_less) return { { x, m, {a.first, rule.nb}, s } , { x, m, {rule.nb, a.second + 1}, s } };
                     else return { {} , {x, m, a, s} };
                  }; break;
                  case 's': {
                     if (rule.is_less) return { { x, m, a, {s.first, rule.nb} } , { x, m, a, {rule.nb, s.second + 1}} };
                     else return { {} , {x, m, a, s} };
                  }; break;
               }
            }

            switch (rule.which_char) {
               case 'x': {
                  if (rule.is_less) return { { {x.first, rule.nb}, m, a, s } , { {rule.nb, x.second} , m, a, s } };
                  else return { { {rule.nb + 1, x.second} , m, a, s } , { {x.first, rule.nb + 1}, m, a, s } };
               }; break;
               case 'm': {
                  if (rule.is_less) return { { x, {m.first, rule.nb}, a, s } , { x, {rule.nb, m.second}, a, s } };
                  else return { { x, {rule.nb + 1, m.second}, a, s } , { x, {m.first, rule.nb + 1}, a, s } };
               }; break;
               case 'a': {
                  if (rule.is_less) return { { x, m, {a.first, rule.nb}, s } , { x, m, {rule.nb, a.second}, s } };
                  else return { { x, m, {rule.nb + 1, a.second}, s } , { x, m, {a.first, rule.nb + 1}, s } };
               }; break;
               case 's': {
                  if (rule.is_less) return { { x, m, a, {s.first, rule.nb} } , { x, m, a, {rule.nb, s.second} } };
                  else return { { x, m, a, {rule.nb + 1, s.second} } , { x, m, a, {s.first, rule.nb + 1} } };
               }; break;
            }
         }; break;
      }


      return { {}, {} };
   }

   size_t nb_combinations() const {
      if (x.second <= x.first || m.second <= m.first || a.second <= a.first || s.second <= s.first) return 0;
      else return (x.second - x.first) * (m.second - m.first) * (a.second - a.first) * (s.second - s.first);
   }

   friend std::ostream& operator<<(std::ostream& os, const Range& in) {
      return os << "x: [" << in.x.first << ", " << in.x.second << "); "
                << "m: [" << in.m.first << ", " << in.m.second << "); "
                << "a: [" << in.a.first << ", " << in.a.second << "); "
                << "s: [" << in.s.first << ", " << in.s.second << "), nb_combinations: " << in.nb_combinations();
   }
};


class WorkflowRange {
public:
   std::vector<Rule> rules{};

   size_t process_range(const std::unordered_map<std::string, WorkflowRange>& workflow_map, Range rg) const {
      size_t ret = 0;
      std::pair<Range, Range> rgs{{}, rg};

      for (const Rule& rule : rules) {
         if (rgs.second.nb_combinations() == 0) { break; }

         //Get the range processed by the rule
         rgs = rgs.second.split_range(rule);

         //Add the number accepted by the rule if it's accepting/rejecting now, or pass on to next workflow
         switch (rule.type) {
            case Rule::Type::LogicAccept: /*fallthrough*/
            case Rule::Type::Accept: {
               if (rule.is_accept) {
                  ret += rgs.first.nb_combinations();
               }
            }; break;
            case Rule::Type::Teleport: /*fallthrough*/
            case Rule::Type::LogicTeleport: {
               ret += workflow_map.at(rule.label).process_range(workflow_map, rgs.first);
            }; break;
         }
      }

      return ret;
   }
};

size_t task_2(std::string_view file) {
   std::string line;
   size_t sum = 0;

   std::unordered_map<std::string, WorkflowRange> workflows{};

   for (const auto line : std::views::split(file, '\n')) {
      if (line.size() == 0) { break; }

      WorkflowRange cur_workflow{};
      size_t ind_lcurly = std::find(line.begin(), line.end(), '{') - line.begin();
      assert(ind_lcurly < line.size());
      const std::string label{line.begin(), line.begin() + ind_lcurly};

      for (const auto cur_predicate
         : std::views::split(std::string_view{line.begin() + ind_lcurly + 1, line.end() - 1}, ','))
      {
         const Rule cur_rule { std::string_view(cur_predicate.begin(), cur_predicate.end()) };
         cur_workflow.rules.push_back(cur_rule);
      }

      workflows[label] = cur_workflow;
   }

   Range start_rg { {1, 4001}, {1, 4001}, {1, 4001}, {1, 4001} };

   return workflows.at("in").process_range(workflows, start_rg);
}
