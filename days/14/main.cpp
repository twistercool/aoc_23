#include "aoc.hpp"

#include <fstream>
#include <unordered_map>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


class Reflector {
public:
   std::vector<std::string> map;

   Reflector() { }

   void RollNorth() {
      for (uint i = 0; i < map.at(0).size(); ++i) {
         size_t index_start = 0;
         size_t nb_stones = 0;
         size_t nb_empty = 0;
         for (uint j = 0; j < map.size(); ++j) {
            const char c = map[j][i];
            switch (c) {
               case 'O': { nb_stones++; } break;
               case '.': { nb_empty++; } break;
               case '#': {
                  if (nb_stones != 0) {
                     for (uint new_j = index_start; new_j < index_start + nb_stones; ++new_j)
                        map[new_j][i] = 'O';
                     for (uint new_j = index_start + nb_stones; new_j < j; ++new_j)
                        map[new_j][i] = '.';
                  }
                  nb_stones = 0;
                  nb_empty = 0;
                  index_start = j + 1;
               } break;
            }
         }
         
         if (nb_stones != 0) { 
            for (uint new_j = index_start; new_j < index_start + nb_stones; ++new_j)
               map[new_j][i] = 'O';
            for (uint new_j = index_start + nb_stones; new_j < map.size(); ++new_j)
               map[new_j][i] = '.';
         }
      }
   }

   void RollSouth() {
      for (uint i = 0; i < map.at(0).size(); ++i) {
         int index_start = map.size()-1;
         size_t nb_stones = 0;
         size_t nb_empty = 0;
         for (int j = map.size() - 1; j >= 0; --j) {
            const char c = map[j][i];
            switch (c) {
               case 'O': { nb_stones++; } break;
               case '.': { nb_empty++; } break;
               case '#': {
                  if (nb_stones != 0) {
                     for (uint new_j = index_start; new_j > index_start - nb_stones; --new_j)
                        map[new_j][i] = 'O';
                     for (int new_j = index_start - nb_stones; new_j > j; --new_j)
                        map[new_j][i] = '.';
                  }
                  nb_stones = 0;
                  nb_empty = 0;
                  index_start = j-1;
               } break;
            }
         }
         
         if (nb_stones != 0) {
            for (uint new_j = index_start; new_j > index_start - nb_stones; --new_j)
               map[new_j][i] = 'O';
            for (int new_j = index_start - nb_stones; new_j >= 0; --new_j)
               map[new_j][i] = '.';
         }
      }
   }

   void RollEast() {
      for (uint i = 0; i < map.size(); ++i) {
         size_t index_start = map[0].size()-1;
         size_t nb_stones = 0;
         size_t nb_empty = 0;
         for (int j = map[i].size() - 1; j >= 0; --j) {
            const char c = map[i][j];
            switch (c) {
               case 'O': { nb_stones++; } break;
               case '.': { nb_empty++; } break;
               case '#': {
                  if (nb_stones != 0) {
                     for (uint new_j = index_start; new_j > index_start - nb_stones; --new_j)
                        map[i][new_j] = 'O';
                     for (int new_j = index_start - nb_stones; new_j > j; --new_j)
                        map[i][new_j] = '.';
                  }
                  nb_stones = 0;
                  nb_empty = 0;
                  index_start = j-1;
               } break;
            }
         }
         
         if (nb_stones != 0) {
            for (uint new_j = index_start; new_j > index_start - nb_stones; --new_j)
               map[i][new_j] = 'O';
            for (int new_j = index_start - nb_stones; new_j >= 0; --new_j)
               map[i][new_j] = '.';
         }
      }
   }
   void RollWest() {
      for (uint i = 0; i < map.size(); ++i) {
         size_t index_start = 0;
         size_t nb_stones = 0;
         size_t nb_empty = 0;
         for (uint j = 0; j < map[0].size(); ++j) {
            const char c = map[i][j];
            switch (c) {
               case 'O': { nb_stones++; } break;
               case '.': { nb_empty++; } break;
               case '#': {
                  if (nb_stones != 0) {
                     for (uint new_j = index_start; new_j < index_start + nb_stones; ++new_j)
                        map[i][new_j] = 'O';
                     for (uint new_j = index_start + nb_stones; new_j < j; ++new_j)
                        map[i][new_j] = '.';
                  }
                  nb_stones = 0;
                  nb_empty = 0;
                  index_start = j+1;
               } break;
            }
         }
         
         if (nb_stones != 0) {
            for (uint new_j = index_start; new_j < index_start + nb_stones; ++new_j)
               map[i][new_j] = 'O';
            for (uint new_j = index_start + nb_stones; new_j < map[0].size(); ++new_j)
               map[i][new_j] = '.';
         }
      }
   }

   void RollAround() {
      RollNorth();
      RollWest();
      RollSouth();
      RollEast();
   }

   size_t GetLoad() const {
      size_t sum = 0;
      for (uint i = 0; i < map.size(); ++i) {
         const size_t load_single = map.size() - i;
         for (uint j = 0; j < map[0].size(); ++j) {
            if (map[i][j] == 'O') {
               sum += load_single;
            }
         }
      }
      return sum;
   }

   friend std::ostream& operator<<(std::ostream& os, const Reflector& a) {
      for (uint i = 0; i < a.map.size(); ++i) {
         for (uint j = 0; j < a.map[0].size(); ++j) {
            os << a.map[i][j];
         }
         os << "\n";
      }
      return os;
   }

   //Assumes a.map.size() == b.map.size(), as well as each elem of the vector
   friend int operator<=>(const Reflector& a, const Reflector& b) {
      for (int i = 0; i < int(a.map.size()); ++i) {
         for (int j = 0; j < int(a.map[0].size()); ++j) {
            if (a.map[i][j] != b.map[i][j]) {
               if (a.map[i][j] > b.map[i][j]) {
                  return i * a.map[0].size() + j;
               } else {
                  return -(i * a.map[0].size() + j);
               }
            }
         }
      }
      return 0;
   }
};

namespace std {
int operator<=>(const std::vector<std::string>& a, const std::vector<std::string>& b) {
   for (int i = 0; i < int(a.size()); ++i) {
      for (int j = 0; j < int(a[0].size()); ++j) {
         if (a[i][j] != b[i][j]) {
            if (a[i][j] > b[i][j]) {
               return i * a[0].size() + j + 1;
            } else {
               return -(i * a[0].size() + j + 1);
            }
         }
      }
   }
   return 0;
}

template<>
struct std::hash<Reflector> {
   size_t operator()(const Reflector& a) const {
      size_t ret = 0x3f23ae;

      for (uint i = 0; i < a.map.size(); i++) {
         ret ^= std::hash<std::string>()(a.map[i]);
      }

      return ret;
   }
};
}

size_t task_1(std::stringstream file) {
   std::string line;
   size_t sum = 0;
   Reflector ref{};
   for (uint i = 0; std::getline(file, line); i++) {
      ref.map.push_back(std::move(line));
   }
   /* std::cout << ref << "\n"; */
   ref.RollNorth();
   /* std::cout << ref; */
   return ref.GetLoad();
}

size_t task_2(std::stringstream file) {
   std::string line;
   size_t sum = 0;
   Reflector ref{};
   for (uint i = 0; std::getline(file, line); i++) {
      ref.map.push_back(std::move(line));
   }

   std::hash<Reflector> ref_hash{};

   std::unordered_map<size_t, size_t> prev_refs{};
   prev_refs[ref_hash(ref)] = 0;
   size_t index_return = 0;
   for (uint i = 1; i <= 1'000'000'000; ++i) {
      ref.RollAround();
      if (prev_refs.contains(ref_hash(ref))) {
         size_t val = prev_refs[ref_hash(ref)];
         size_t cycle_len = i - val;
         size_t mod = (1'000'000'000 - i) % cycle_len;
         for (uint j = 0; j < mod; j++) ref.RollAround();
         return ref.GetLoad();
      }
      prev_refs[ref_hash(ref)] = i;
   }

   return ref.GetLoad();
}
