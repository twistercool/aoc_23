#include "aoc.hpp"

#include <fstream>
#include <map>
#include <algorithm>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

enum HandType {
   HighCard = 0,
   Pair,
   TwoPair,
   ThreeOfAKind,
   FullHouse,
   FourOfAKind,
   FiveOfAKind
};

class Hand {
public:
   std::array<uint8_t, 5> cards;
   uint32_t bid;
   HandType type;


   //Assumes correct inputs
   Hand(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e, uint32_t bid): bid(bid) {
      cards[0] = a; cards[1] = b; cards[2] = c; cards[3] = d; cards[4] = e;

      std::map<uint8_t, uint8_t> cards_mapped{};
      for (const auto e: cards) {
         cards_mapped[e]++;
      }

      switch (cards_mapped.size()) {
         case 5: { type = HandType::HighCard; } break;
         case 4: { type = HandType::Pair; } break;
         case 3: {
            //Either aaabc or aabbc
            const uint8_t nb_of_first = cards_mapped.cbegin()->second;
            if (nb_of_first == 3) { type = HandType::ThreeOfAKind; }
            else if (nb_of_first == 2) { type = HandType::TwoPair; }
            else {
               const uint8_t nb_of_sec = (++cards_mapped.cbegin())->second;
               if (nb_of_sec == 3 || nb_of_sec == 1) type = HandType::ThreeOfAKind;
               else type = HandType::TwoPair;
            }
         } break;
         case 2: {
            //Either or aaaab, aaabb
            const uint8_t nb_of_first = cards_mapped.cbegin()->second;
            if (nb_of_first == 4 || nb_of_first == 1) type = HandType::FourOfAKind;
            else type = HandType::FullHouse;
         } break;
         case 1: { type = HandType::FiveOfAKind; } break;
      }
   }

   static std::optional<Hand> ParseStr(const std::string_view str) {
      std::array<uint8_t, 5> c;
      if (str.size() < 7 || str[5] != ' ') {
         return {};
      }
      uint i = 0;
      for (; i < 5; i++) {
         switch(str[i]) {
            case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': {
               c[i] = str[i] - '0';
            } break;
            case 'T': c[i] = 10; break;
            case 'J': c[i] = 11; break;
            case 'Q': c[i] = 12; break;
            case 'K': c[i] = 13; break;
            case 'A': c[i] = 14; break;
            default: return {};
         }
      }
      i++;
      uint32_t bid = 0;
      while (i < str.size()) {
         bid = bid * 10 + str[i] - '0';
         i++;
      }

      return Hand(c[0], c[1], c[2], c[3], c[4], bid);
   }

   friend int operator<=>(const Hand& a, const Hand& b) {
      int8_t diff = a.type - b.type;
      if (diff != 0) return diff;

      for (uint i = 0; i < 5; i++) {
         diff = a.cards[i] - b.cards[i];
         if (diff != 0) return diff;
      }

      return 0;
   }

   friend std::ostream& operator<<(std::ostream& os, const Hand& in) {
      os << "Hand: ";
      for (const auto e: in.cards) {
         os << size_t{e} << " ";
      } 
      os << "Bid: " << in.bid << " ";
      switch (in.type) {
         case HighCard: { os << "HighCard"; } break;
         case Pair: { os << "Pair"; } break;
         case TwoPair: { os << "TwoPair"; } break;
         case ThreeOfAKind: { os << "ThreeOfAKind"; } break;
         case FullHouse: { os << "FullHouse"; } break;
         case FourOfAKind: { os << "FourOfAKind"; } break;
         case FiveOfAKind: { os << "FiveOfAKind"; } break;
      }
      return os;
   }
};

class HandJoker {
public:
   std::array<uint8_t, 5> cards;
   uint32_t bid;
   HandType type;


   //Assumes correct inputs
   HandJoker(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e, uint32_t bid): bid(bid) {
      cards[0] = a; cards[1] = b; cards[2] = c; cards[3] = d; cards[4] = e;

      std::map<uint8_t, uint8_t> cards_mapped{};
      for (const auto e: cards) {
         cards_mapped[e]++;
      }
      
      if (cards_mapped.contains(1)) {
         size_t nb_jokers = cards_mapped[1];
         cards_mapped.erase(1);
         switch (nb_jokers) {
            case 5: case 4: { type = HandType::FiveOfAKind; } break;
            case 3: {
               //if jjjaa then five of a kind, else four of a kind
               if (cards_mapped.size() == 1) {
                  type = HandType::FiveOfAKind;
               } else {
                  type = HandType::FourOfAKind;
               }
            } break;
            case 2: {
               //either jjabc, jjaab or jjaaa
               if (cards_mapped.size() == 1) {
                  type = HandType::FiveOfAKind;
               } else if (cards_mapped.size() == 2) {
                  type = HandType::FourOfAKind;
               } else {
                  type = HandType::ThreeOfAKind;
               }
            } break;
            case 1: {
               //either jabcd, jaabc, jaaab, jaabb, jaaaa
               if (cards_mapped.size() == 1) {
                  type = HandType::FiveOfAKind;
               } else if (cards_mapped.size() == 2) {
                  const uint8_t nb_of_first = cards_mapped.cbegin()->second;
                  if (nb_of_first == 3 || nb_of_first == 1) {
                     type = HandType::FourOfAKind;
                  } else {
                     type = HandType::FullHouse;
                  }
               } else if (cards_mapped.size() == 3) {
                  type = HandType::ThreeOfAKind;
               } else {
                  type = HandType::Pair;
               }

            } break;
         }
      } else {
         switch (cards_mapped.size()) {
            case 5: { type = HandType::HighCard; } break;
            case 4: { type = HandType::Pair; } break;
            case 3: {
               //Either aaabc or aabbc
               const uint8_t nb_of_first = cards_mapped.cbegin()->second;
               if (nb_of_first == 3) { type = HandType::ThreeOfAKind; }
               else if (nb_of_first == 2) { type = HandType::TwoPair; }
               else {
                  const uint8_t nb_of_sec = (++cards_mapped.cbegin())->second;
                  if (nb_of_sec == 3 || nb_of_sec == 1) type = HandType::ThreeOfAKind;
                  else type = HandType::TwoPair;
               }
            } break;
            case 2: {
               //Either or aaaab, aaabb
               const uint8_t nb_of_first = cards_mapped.cbegin()->second;
               if (nb_of_first == 4 || nb_of_first == 1) type = HandType::FourOfAKind;
               else type = HandType::FullHouse;
            } break;
            case 1: { type = HandType::FiveOfAKind; } break;
         }
      }
   }

   static std::optional<HandJoker> ParseStr(const std::string_view str) {
      std::array<uint8_t, 5> c;
      if (str.size() < 7 || str[5] != ' ') {
         return {};
      }
      uint i = 0;
      for (; i < 5; i++) {
         switch(str[i]) {
            case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': {
               c[i] = str[i] - '0';
            } break;
            case 'J': c[i] = 1; break;
            case 'T': c[i] = 10; break;
            case 'Q': c[i] = 12; break;
            case 'K': c[i] = 13; break;
            case 'A': c[i] = 14; break;
            default: return {};
         }
      }
      i++;
      uint32_t bid = 0;
      while (i < str.size()) {
         bid = bid * 10 + str[i] - '0';
         i++;
      }

      return HandJoker(c[0], c[1], c[2], c[3], c[4], bid);
   }

   friend int operator<=>(const HandJoker& a, const HandJoker& b) {
      int8_t diff = a.type - b.type;
      if (diff != 0) return diff;

      for (uint i = 0; i < 5; i++) {
         diff = a.cards[i] - b.cards[i];
         if (diff != 0) return diff;
      }

      return 0;
   }

   friend std::ostream& operator<<(std::ostream& os, const HandJoker& in) {
      os << "Hand: ";
      for (const auto e: in.cards) {
         os << size_t{e} << " ";
      } 
      os << "Bid: " << in.bid << " ";
      switch (in.type) {
         case HighCard: { os << "HighCard"; } break;
         case Pair: { os << "Pair"; } break;
         case TwoPair: { os << "TwoPair"; } break;
         case ThreeOfAKind: { os << "ThreeOfAKind"; } break;
         case FullHouse: { os << "FullHouse"; } break;
         case FourOfAKind: { os << "FourOfAKind"; } break;
         case FiveOfAKind: { os << "FiveOfAKind"; } break;
      }
      return os;
   }
};

size_t task_1(std::stringstream file) {
   std::string line;

   std::vector<Hand> hands;
   hands.reserve(1000);

   for (uint i = 0; std::getline(file, line); i++) {
      std::optional<Hand> o_hand = Hand::ParseStr(std::string_view{line});
      if (!o_hand) { die("Failed to parse hand"); }
      /* std::cout << *o_hand << "\n"; */
      hands.push_back(*o_hand);
   }

   std::sort(hands.begin(), hands.end());

   size_t sum = 0;
   for (uint i = 0; i < hands.size(); i++) {
      /* std::cout << hands[i] << "\n"; */
      sum += (i + 1) * hands[i].bid;
   }

   return sum;
}

size_t task_2(std::stringstream file) {
   std::string line;

   std::vector<HandJoker> hands;
   hands.reserve(8);

   for (uint i = 0; std::getline(file, line); i++) {
      std::optional<HandJoker> o_hand = HandJoker::ParseStr(std::string_view{line});
      if (!o_hand) { die("Failed to parse hand"); }
      /* std::cout << *o_hand << "\n"; */
      hands.push_back(*o_hand);
   }

   std::sort(hands.begin(), hands.end());

   size_t sum = 0;
   for (uint i = 0; i < hands.size(); i++) {
      /* std::cout << hands[i] << "\n"; */
      sum += (i + 1) * hands[i].bid;
   }

   return sum;
}
