#include "aoc.hpp"

#include <vector>
#include <iostream>
#include <numeric>
#include <fstream>
#include <optional>
#include <unordered_map>
#include <unordered_set>
#include <string_view>
#include <ranges>


//Coords start at (0,0), mean (x, y)
using Coord = std::pair<int32_t, int32_t>;

//Defines hash for Coord (needed for unordered_set and unordered_map)
struct Coord_hash {
    std::size_t operator () (const Coord &p) const {
        return std::hash<size_t>{}(((size_t)p.first << 16) + (size_t)p.second);
    }
};

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }
   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   /* Timer t1; */
   size_t task_1_ret = task_1(std::stringstream{file});
   /* std::cout << t1.TimeMicros() << "\n"; */
   /* Timer t2; */
   size_t task_2_ret = task_2(std::stringstream{file});
   /* std::cout << t2.TimeMicros() << "\n"; */
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

size_t task_1bis(const std::string_view sv) {
   size_t line_len = 0;
   size_t cur = 0;
   while (cur < sv.size() && sv[cur] != '\n') {
      line_len++;
   }
   size_t cur_line = 0;


   const auto sum_numbers_around = [&](size_t line, size_t x) -> size_t {
      size_t top_left = 0, top_right = 0, left = 0, right = 0, btm_left = 0, btm_right = 0;

      //Check left
      if (isdigit(sv[line * line_len + x - 1])) {
         size_t mult = 1;
         for (int j = line * line_len + x - 1; j >= 0 && isdigit(sv[j]); j--) {
            left += mult * (sv[j] - '0');
            mult *= 10;
         }
      }
      //Check right
      if (isdigit(sv[line * line_len + x + 1])) {
         for (uint j = line * line_len + x + 1; j < sv.size() && isdigit(sv[j]); j++) {
            right = right * 10 + sv[j] - '0';
         }
      }

      //Check top_right
      /* if (line > 0 && isdigit(sv[line * line_len + x + 1])) { */
      /*    for (uint j = line * line_len + x + 1; j < sv.size() && isdigit(sv[j]); j++) { */
      /*       right = right * 10 + sv[j] - '0'; */
      /*    } */
      /* } */

      return top_left + top_right + left + right + btm_left + btm_right;
   };

   size_t sum = 0;
   for (const auto line: std::ranges::split_view(sv, '\n')) {
      for (uint i = 0; i < line.size(); i++) {
         if (line[i] != '.' && !isdigit(line[i])) {
            size_t nb_nbs = 0;
            //parse numbers around
            sum += sum_numbers_around(cur_line, i);
         }
      }
      cur_line++;
   }

   return sum;
}

size_t task_1(std::stringstream file) {

   /* std::vector<std::string> in; */
   /* std::string l; */
   /* while (getline(file, l)) { */
   /*    in.push_back(std::move(l)); */
   /* } */

   /* size_t y_len = in.size(); */
   /* if (y_len == 0) { */
   /*    std::cerr << "Incorrect input" << std::endl; */
   /*    std::exit(1); */
   /* } */
   /* size_t x_len = in[0].size(); */
   /* auto is_neighbour_part = [&](const Coord& c){ */
   /*    if (c.first >= 0 && c.) { */
         
   /*    } */
   /*    return false; */
   /* }; */

   std::unordered_set<Coord, Coord_hash> coords_part_nbs{};
   std::string line;
   for (uint y = 0; std::getline(file, line); y++) {
      for (uint x = 0; x < line.size(); x++) {
         if (!isdigit(line[x]) && line[x] != '.') {
            /* std::cout << "Found weird thing at " << x << " " << y << "\n"; */
            coords_part_nbs.insert({x - 1, y - 1});
            coords_part_nbs.insert({x, y - 1});
            coords_part_nbs.insert({x + 1, y - 1});
            coords_part_nbs.insert({x - 1, y});
            coords_part_nbs.insert({x + 1, y});
            coords_part_nbs.insert({x - 1, y + 1});
            coords_part_nbs.insert({x, y + 1});
            coords_part_nbs.insert({x + 1, y + 1});
         }
      }
   }

   file.clear(); file.seekg(std::ios::beg);

   size_t sum = 0;
   for (uint y = 0; std::getline(file, line); y++) {
      for (uint x = 0; x < line.size(); x++) {
         if (isdigit(line[x])) {
            size_t cur_nb = 0;
            bool is_part_nb = false;
            for (; x < line.size() && isdigit(line[x]); x++) {
               if (!is_part_nb && coords_part_nbs.contains({x, y})) {
                  is_part_nb = true;
               }
               cur_nb = cur_nb * 10 + (line[x] - '0');
            }
            if (is_part_nb) { sum += cur_nb; }
         }
      }
   }
   return sum;
}

size_t task_2(std::stringstream file) {
   std::unordered_map<Coord, std::vector<uint>, Coord_hash> gear_nbs;
   std::unordered_map<Coord, Coord, Coord_hash> coord_to_gear;

   std::string line;
   for (uint y = 0; std::getline(file, line); y++) {
      for (uint x = 0; x < line.size(); x++) {
         if (line[x] == '*') {
            gear_nbs.insert({{x, y}, std::vector<uint>{}});

            coord_to_gear.insert({{x - 1, y - 1}, {x, y}});
            coord_to_gear.insert({{x, y - 1},     {x, y}});
            coord_to_gear.insert({{x + 1, y - 1}, {x, y}});
            coord_to_gear.insert({{x - 1, y},     {x, y}});
            coord_to_gear.insert({{x + 1, y},     {x, y}});
            coord_to_gear.insert({{x - 1, y + 1}, {x, y}});
            coord_to_gear.insert({{x, y + 1},     {x, y}});
            coord_to_gear.insert({{x + 1, y + 1}, {x, y}});
         }
      }
   }

   file.clear(); file.seekg(std::ios::beg);

   for (uint y = 0; std::getline(file, line); y++) {
      for (uint x = 0; x < line.size(); x++) {
         if (isdigit(line[x])) {
            size_t cur_nb = 0;
            std::optional<Coord> gear_next_to{};
            for (; x < line.size() && isdigit(line[x]); x++) {
               if (!gear_next_to && coord_to_gear.contains({x, y})) {
                  gear_next_to = coord_to_gear.at({x, y});
               }
               cur_nb = cur_nb * 10 + (line[x] - '0');
            }
            if (gear_next_to) {
               gear_nbs.at(*gear_next_to).push_back(cur_nb);
            }
         }
      }
   }

   size_t sum = 0;
   for (auto it: gear_nbs) {
      auto vec = it.second;
      if (vec.size() > 1) {
         sum += std::accumulate(vec.cbegin(), vec.cend(), size_t{1}, [](const size_t a, const size_t b){ return a * b; });
      }
   }

   return sum;
}
