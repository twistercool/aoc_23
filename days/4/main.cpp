#include "aoc.hpp"

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


size_t task_1(std::stringstream file) {
   std::string line;
   size_t sum = 0;
   for (uint i = 0; std::getline(file, line); i++) {
      size_t ind = line.find(':');
      if (ind == std::string::npos) { die("Incorrect input"); }

      for (;!isdigit(line[ind]); ind++);

      std::vector<size_t> win_nbs{};
      std::vector<size_t> my_nbs{};
      bool parsing_win_nbs = true;
      while (ind < line.size()) {
         size_t cur_nb = 0;
         for (;isdigit(line[ind]); ind++) {
            cur_nb = cur_nb * 10 + (line[ind] - '0');
         }
         if (parsing_win_nbs) { win_nbs.push_back(cur_nb); }
         else { my_nbs.push_back(cur_nb); }
         for (;!isdigit(line[ind]); ind++) {
            if (parsing_win_nbs && line[ind] == '|') {
               parsing_win_nbs = false;
            }
         };
      }

      size_t cur_sum = 0;
      for (auto nb: my_nbs) {
         if (std::find(win_nbs.cbegin(), win_nbs.cend(), nb) != win_nbs.cend()) {
            if (cur_sum) { cur_sum *= 2; }
            else { cur_sum = 1; }
         }
      }
      sum += cur_sum;
   }
   return sum;
}

size_t task_2(std::stringstream file) {
   std::string line;

   std::vector<size_t> nb_winning{};

   size_t sum = 0;
   for (uint i = 0; std::getline(file, line); i++) {
      size_t ind = line.find(':');
      if (ind == std::string::npos) { die("Incorrect input"); }

      for (;!isdigit(line[ind]); ind++);

      std::vector<size_t> win_nbs{};
      std::vector<size_t> my_nbs{};
      bool parsing_win_nbs = true;
      while (ind < line.size()) {
         size_t cur_nb = 0;
         for (;isdigit(line[ind]); ind++) {
            cur_nb = cur_nb * 10 + (line[ind] - '0');
         }
         if (parsing_win_nbs) { win_nbs.push_back(cur_nb); }
         else { my_nbs.push_back(cur_nb); }
         for (;!isdigit(line[ind]); ind++) {
            if (parsing_win_nbs && line[ind] == '|') {
               parsing_win_nbs = false;
            }
         };
      }

      size_t cur_sum = 0;
      for (auto nb: my_nbs) {
         if (std::find(win_nbs.cbegin(), win_nbs.cend(), nb) != win_nbs.cend()) {
            cur_sum++;
         }
      }

      while (nb_winning.size() <= i + cur_sum) {
         nb_winning.push_back(1);
      }
      for (uint j = 1; j <= cur_sum; j++) {
         nb_winning[i + j] += nb_winning[i];
      }
      sum += nb_winning[i];
   }

   return sum;
}
