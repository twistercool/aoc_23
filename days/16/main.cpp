#include "aoc.hpp"

#include <fstream>
#include <algorithm>
#include <numeric>
#include <bitset>
#include <functional>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

//(x, y)
using Coord = std::pair<int32_t, int32_t>;

/* enum Polarised: uint8_t { */
/*    None = 0, */
/*    Up = 1 << 1, */
/*    Right = 1 << 2, */
/*    Down = 1 << 3, */
/*    Left = 1 << 4 */
/* }; */

/* inline Polarised operator|(const Polarised a, const Polarised b) { */
/*    return static_cast<Polarised>(static_cast<uint8_t>(a) | static_cast<uint8_t>(b)); */
/* } */
/* inline Polarised& operator|=(Polarised& a, const Polarised b) { */
/*    return (Polarised&)((uint8_t&)(a) |= static_cast<uint8_t>(b)); */
/* } */

class Mirror {
public:
   std::vector<std::string> map;


   size_t GetEnergised(Coord start_pos, int start_dx, int start_dy) {
      std::vector<std::vector<std::bitset<4>>> arr(map.size(), std::vector(map.at(0).size(), std::bitset<4>{0}));

      //dx: -1: left/ 1: right, dy: -1: up/ 1: down
      std::function<void(Coord, int, int)> fire_beam;
      fire_beam = [&](Coord cur_pos, int dx, int dy) -> void {
         for (; cur_pos.first >= 0
            && cur_pos.second >= 0
            && cur_pos.first < int(map.at(0).size())
            && cur_pos.second < int(map.size()); cur_pos.first += dx, cur_pos.second += dy) {
            /* std::cout << "pos: " << cur_pos.first << ":" << cur_pos.second << " " */
            /* << "dir: " << dx << ":" << dy << "\n"; */
            if (dx == 1 && dy == 0) {
               if (arr[cur_pos.second][cur_pos.first][0]) return;
               arr[cur_pos.second][cur_pos.first].set(0);
            }
            else if (dx ==-1 && dy == 0) {
               if (arr[cur_pos.second][cur_pos.first][1]) return;
               arr[cur_pos.second][cur_pos.first].set(1);
            }
            else if (dx == 0 && dy == 1) {
               if (arr[cur_pos.second][cur_pos.first][2]) return;
               arr[cur_pos.second][cur_pos.first].set(2);
            }
            else {
               if (arr[cur_pos.second][cur_pos.first][3]) return;
               arr[cur_pos.second][cur_pos.first].set(3);
            }

            switch (map[cur_pos.second][cur_pos.first]) {
               case '.': break;
               case '/': {
                  if      (dx == 1 && dy == 0) { dx = 0; dy =-1; }
                  else if (dx ==-1 && dy == 0) { dx = 0; dy = 1; }
                  else if (dx == 0 && dy == 1) { dx =-1; dy = 0; }
                  else                         { dx = 1; dy = 0; }
               } break;
               case '\\': {
                  if      (dx == 1 && dy == 0) { dx = 0; dy = 1; }
                  else if (dx ==-1 && dy == 0) { dx = 0; dy =-1; }
                  else if (dx == 0 && dy == 1) { dx = 1; dy = 0; }
                  else                         { dx =-1; dy = 0; }
               } break;
               case '|': {
                  if (dy != 0) { break; }
                  dx = 0; dy = -1;
                  fire_beam(cur_pos, 0, 1);
               } break;
               case '-': {
                  if (dx != 0) { break; }
                  dx = -1; dy = 0;
                  fire_beam(cur_pos, 1, 0);
               } break;
               default: std::cerr << "Got '" << map[cur_pos.second][cur_pos.first] << "\n";
            }

         }
      };

      fire_beam(start_pos, start_dx, start_dy);

      size_t sum = std::accumulate(arr.cbegin(), arr.cend(), size_t{0}, [](size_t acc, const std::vector<std::bitset<4>>& in) {
         return acc + std::count_if(in.cbegin(), in.cend(), [](std::bitset<4> a) {
            return a.any();
         });
      });
      return sum;
      return 0;
   }
};

size_t task_1(std::stringstream file) {
   std::string line;
   size_t sum = 0;
   Mirror m{};
   for (uint i = 0; std::getline(file, line); i++) {
      m.map.push_back(line);
   }
   return m.GetEnergised(Coord{0, 0}, 1, 0);
}

size_t task_2(std::stringstream file) {
   std::string line;
   size_t sum = 0;
   Mirror m{};
   for (uint i = 0; std::getline(file, line); i++) {
      m.map.push_back(line);
   }

   size_t map_len = m.map.size();
   size_t line_len = m.map[0].size();
   size_t max = 0;
   for (uint i = 0; i < line_len; i++) {
      size_t cur = m.GetEnergised(Coord{i, 0}, 0, 1);
      if (cur > max) { max = cur; }
      cur = m.GetEnergised(Coord{i, map_len-1}, 0, -1);
      if (cur > max) { max = cur; }
   }
   for (uint i = 0; i < map_len; i++) {
      size_t cur = m.GetEnergised(Coord{0, i}, 1, 0);
      if (cur > max) { max = cur; }
      cur = m.GetEnergised(Coord{line_len-1, i}, -1, 0);
      if (cur > max) { max = cur; }
   }

   return max;
}
