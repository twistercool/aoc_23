#include "aoc.hpp"

#include <fstream>
#include <optional>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout
   << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}



bool is_reflection_horizontal(const std::vector<std::string>& map, size_t index) {
   for (uint i = 0; i < map.size(); ++i) {
      for (int j = index; j >= 0; j--) {
         const size_t reverse_j = 2 * index - j + 1;
         if (reverse_j < map.at(i).size()) {
            if (map.at(i).at(j) != map.at(i).at(reverse_j)) {
               return false;
            }
         }
      }
   }
   return true;
}

bool is_reflection_vertical(const std::vector<std::string>& map, size_t index) {
   for (uint i = 0; i < map.at(0).size(); ++i) {
      for (int j = index; j >= 0; j--) {
         const size_t reverse_j = 2 * index - j + 1;
         if (reverse_j < map.size()) {
            if (map.at(j).at(i) != map.at(reverse_j).at(i)) {
               return false;
            }
         }
      }
   }
   return true;
}

//Coord is (x, y)
using Coord = std::pair<size_t, size_t>;
std::optional<Coord> get_reflection_horizontal_smudge(const std::vector<std::string>& map, size_t index) {
   std::optional<Coord> first_diff{};
   for (uint i = 0; i < map.size(); ++i) {
      for (int j = index; j >= 0; j--) {
         const size_t reverse_j = 2 * index - j + 1;
         if (reverse_j < map.at(i).size()) {
            if (map.at(i).at(j) != map.at(i).at(reverse_j)) {
               if (!first_diff) {

                  first_diff = {j, i};
               } else {
                  return {};
               }
            }
         }
      }
   }
   return first_diff;
}

std::optional<Coord> get_reflection_vertical_smudge(const std::vector<std::string>& map, size_t index) {
   std::optional<Coord> first_diff{};
   for (uint i = 0; i < map.at(0).size(); ++i) {
      for (int j = index; j >= 0; j--) {
         const size_t reverse_j = 2 * index - j + 1;
         if (reverse_j < map.size()) {
            if (map.at(j).at(i) != map.at(reverse_j).at(i)) {
               if (!first_diff) {
                  first_diff = {i, j};
               } else {
                  return {};
               }
            }
         }
      }
   }
   return first_diff;
}

class Pattern {
public:
   std::vector<std::string> map;

   Pattern() {
   }

   size_t Score() const {
      for (uint i = 0; i < map.at(0).size() - 1; ++i) {
         bool is_ref_h = is_reflection_horizontal(map, i);
         if (is_ref_h) {
            return i + 1;
         }
      }

      for (uint i = 0; i < map.size() - 1; ++i) {
         bool is_ref_v = is_reflection_vertical(map, i);
         if (is_ref_v) {
            return 100 * (i+1);
         }
         
      }

      return 0;
   }

   size_t ScoreWithSmudge() {
      for (uint i = 0; i < map.at(0).size() - 1; ++i) {
         const std::optional<Coord> smudge = get_reflection_horizontal_smudge(map, i);
         if (smudge) {

            return i + 1;
         }
      }

      for (uint i = 0; i < map.size() - 1; ++i) {
         const std::optional<Coord> smudge = get_reflection_vertical_smudge(map, i);
         if (smudge) {

            return 100 * (i+1);
         }
         
      }

      std::cerr << "Didn't find a reflection\n";
      return 0;
   }
};

size_t task_1(std::stringstream file) {
   std::string line;

   std::vector<Pattern> patterns{Pattern{}};
   for (uint i = 0; std::getline(file, line); i++) {
      if (line.size() == 0) {
         patterns.push_back(Pattern{});
         continue;
      }
      patterns.back().map.push_back(std::move(line));
   }
   

   size_t sum = 0;
   for (const Pattern& p : patterns) {
      sum += p.Score();
   }

   return sum;
}

size_t task_2(std::stringstream file) {
   std::string line;

   std::vector<Pattern> patterns{Pattern{}};
   for (uint i = 0; std::getline(file, line); i++) {
      if (line.size() == 0) {
         patterns.push_back(Pattern{});
         continue;
      }
      patterns.back().map.push_back(std::move(line));
   }
   

   size_t sum = 0;
   for (Pattern& p : patterns) {
      sum += p.ScoreWithSmudge();
   }

   return sum;
}
