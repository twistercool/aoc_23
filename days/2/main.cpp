#include "aoc.hpp"

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <optional>


size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

class Play {
public:
   size_t nb_red;
   size_t nb_green;
   size_t nb_blue;

   static std::optional<Play> parse(const std::string_view in) noexcept {
      size_t r = 0, g = 0, b = 0;
   
      size_t start = 0, com = in.find(',', start);
      bool exit = false;
      while (!exit) {
         if (com == std::string::npos) {
            com = in.size();
            exit = true;
         }
         const std::string_view cur = in.substr(start, com);
         size_t nb = 0;
         uint i = 0;

         //Parse digit
         for (; i < cur.size() && !isdigit(cur[i]); i++)
            ;
         for (; i < cur.size() && isdigit(cur[i]); i++) {
            nb = nb * 10 + (cur[i] - '0');
         }
         //Parse colour
         if (cur.find("red") != std::string::npos) { r = nb; }
         else if (cur.find("green") != std::string::npos) { g = nb; }
         else { b = nb; }

         if (exit) { break; }
      
         start = com + 1;
         com = in.find(',', start);
      }
   
      return Play(r, g, b);
   }

   friend std::ostream& operator<<(std::ostream& os, const Play& in) {
      return os << "red: " << in.nb_red << "; green: " << in.nb_green << "blue: " << in.nb_blue;
   }

private:
   Play(size_t r, size_t g, size_t b):
      nb_red(r), nb_green(g), nb_blue(b) {}
};

class Game {
public:
   std::vector<Play> plays;

   static std::optional<Game> parse(const std::string_view in) noexcept {
      size_t ind = in.find(":");
      if (ind == std::string::npos) { return {}; }
   
      std::vector<Play> cur_plays;
      size_t start_ind = ind;
      size_t comma_ind = in.find(';');
      bool exit = false;
      while (!exit) {
         if (comma_ind == std::string::npos) {
            comma_ind = in.size();
            exit = true;
         }
         const std::string_view cur = in.substr(start_ind, comma_ind);

         auto play_opt = Play::parse(cur);
         if (!play_opt) {
            std::cerr << "Failed to make Play from " << cur << std::endl;
            std::exit(1);
         }
         cur_plays.push_back(*play_opt);

         if (exit) { break; }

         start_ind = comma_ind + 1;
         comma_ind = in.find(';', start_ind);
      }

      return Game(cur_plays);
   }

   size_t Power() const {
      size_t r = 0, g = 0, b = 0;
      for (auto p: plays) {
         if (p.nb_red > r)
            r = p.nb_red;
         if (p.nb_green > g)
            g = p.nb_green;
         if (p.nb_blue > b)
            b = p.nb_blue;
      }

      return r * g * b;
   }

   friend std::ostream& operator<<(std::ostream& os, const Game& in) {
      for (auto elem: in.plays) {
         os << elem << " ";
      }
      
      return os;
   }
private:
   Game(std::vector<Play> cur_plays): plays(cur_plays) { }
};

size_t task_1(std::stringstream file) {
   std::string line;

   size_t sum = 0;
   for (uint id = 1; std::getline(file, line); id++) {
      auto game_opt = Game::parse(line);
      if (game_opt == std::nullopt) {
         std::cerr << "Failed to make Game from " << line << std::endl;
         std::exit(1);
      }
      Game game = *game_opt;

      bool is_possible = true;
      for (auto e: game.plays) {
         if (e.nb_red > 12 || e.nb_green > 13 || e.nb_blue > 14) {
            is_possible = false; break;
         }
      }
      if (is_possible) { sum += id; }
      
   }

   return sum;
}

size_t task_2(std::stringstream file) {
   size_t sum = 0;

   std::string line;
   for (uint id = 1; std::getline(file, line); id++) {
      auto game_opt = Game::parse(line);
      if (game_opt == std::nullopt) {
         std::cerr << "Failed to make Game from " << line << std::endl;
         std::exit(1);
      }
      Game game = *game_opt;
      
      sum += game.Power();
   }
   return sum;
}
