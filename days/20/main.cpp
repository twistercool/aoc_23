#include "aoc.hpp"

#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <ranges>
#include <numeric>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


enum class Type {
   Output,
   Button,
   Broadcast,
   Conjunction,
   FlipFlop,
};
class Module {
public:
   Type type = Type::Output;
   bool is_high = false;

   /* void ReceivePulse(bool is_low) { */
   /*    switch (type) { */

   /*    } */
   /* } */
   /* virtual void ReceivePulse() = 0; */
   /* virtual void SendPulse() = 0; */
   bool operator==(const Module&) const = default;
};

class Pulse {
public:
   std::string sender;
   std::string dest;
   bool is_high;

   explicit Pulse(std::string sender, std::string receiver, bool is_high) : sender(sender), dest(receiver), is_high(is_high) {}
};

namespace std {
template<>
struct hash<Pulse> {
   size_t operator()(const Pulse& in) const {
      return std::hash<std::string>{}(in.dest) ^ std::hash<std::string>{}(in.sender) ^ in.is_high;
   }
};
}

size_t task_1(std::stringstream file) {
   std::string line;
   size_t sum = 0;

   std::unordered_map<std::string, Module> mod_from_label { {"button", Module{ .type = Type::Button }} };
   std::unordered_map<std::string, std::vector<std::string>> mod_dests { {"button", {"broadcaster"}} };

   for (uint i = 0; std::getline(file, line); i++) {
      Module cur;
      size_t ind = 0;
      if (line.front() == '%') {
         cur.type = Type::FlipFlop;
         ind++;
      } else if (line.front() == '&') {
         cur.type = Type::Conjunction;
         ind++;
      } else {
         cur.type = Type::Broadcast;
      }
      std::string label;
      for (; ind < line.size() && line[ind] != ' '; ind++) {
         label.push_back(line[ind]);
      }
      ind += 4;
      mod_from_label.insert_or_assign(label, cur);

      std::vector<std::string> mods;
      while (ind < line.size()) {
         std::string cur_label;
         for (; ind < line.size() && line[ind] != ','; ind++) {
            cur_label.push_back(line[ind]);
         }
         mods.push_back(cur_label);
         ind+=2;
      }
      //Defaults outputs to "Type::Output" if doesn't exist
      for (const auto& dest : mods) {
         if (!mod_from_label.contains(dest)) {
            mod_from_label.insert_or_assign(dest, Module{ .type = Type::Output });
         }
      }
      mod_dests.insert_or_assign(label, mods);
   }

   std::unordered_map<std::string, std::vector<Pulse>> conjunction_mem{};

   for (const auto& [sender_label, dest_labels] : mod_dests) {
      for (const auto& cur_dest_label : dest_labels) {
         if (mod_from_label.at(cur_dest_label).type == Type::Conjunction) {
            if (!conjunction_mem.contains(cur_dest_label))
               conjunction_mem.insert_or_assign(cur_dest_label, std::vector<Pulse>{ Pulse{sender_label, cur_dest_label, false}});
            else
               conjunction_mem.at(cur_dest_label).push_back(Pulse{sender_label, cur_dest_label, false});
         }
      }
   }

   size_t sum_high = 0;
   size_t sum_low = 0;

   std::deque<Pulse> pulses{ Pulse{"button", "broadcaster", false} };

   for (uint i = 0; i < 1000; i++, pulses.push_back(Pulse{"button", "broadcaster", false})) {
      while (pulses.size() > 0) {
         Pulse cur_pulse = pulses.front();
         pulses.pop_front();

         if (cur_pulse.is_high) {
            sum_high++;
         } else {
            sum_low++;
         }

         //module gets the pulse, and send other pulses
         Module& cur_mod = mod_from_label.at(cur_pulse.dest);
         switch (cur_mod.type) {
            case Type::Button: { } break;
            case Type::Broadcast: {
               for (const auto& dest : mod_dests.at(cur_pulse.dest))
                  pulses.push_back(Pulse{cur_pulse.dest, dest, cur_pulse.is_high});
            } break;
            case Type::Conjunction: {
               std::vector<Pulse>& prev_pulses = conjunction_mem.at(cur_pulse.dest);
               for (auto& p : prev_pulses) {
                  if (p.sender == cur_pulse.sender) {
                     p.is_high = cur_pulse.is_high;
                     break;
                  }
               }

               bool pulse_to_send = !std::all_of(prev_pulses.begin(), prev_pulses.end(), [](const Pulse& p) {
                  return p.is_high == true;
               });

               for (const auto& dest : mod_dests.at(cur_pulse.dest))
                  pulses.push_back(Pulse{ cur_pulse.dest, dest, pulse_to_send });
            } break;
            case Type::FlipFlop: {
               if (cur_pulse.is_high == false) {
                  cur_mod.is_high = !cur_mod.is_high;
                  for (const auto& dest : mod_dests.at(cur_pulse.dest))
                     pulses.push_back(Pulse{ cur_pulse.dest, dest , cur_mod.is_high });
               }
            } break;
            case Type::Output: {} break;
         }
      }
   }


   return sum_high * sum_low;
}

size_t task_2(std::stringstream file) {
   std::string line;
   size_t sum = 0;

   std::unordered_map<std::string, Module> mod_from_label { {"button", Module{ .type = Type::Button }} };
   std::unordered_map<std::string, std::vector<std::string>> mod_dests { {"button", std::vector<std::string>{"broadcaster"}} };

   for (uint i = 0; std::getline(file, line); i++) {
      Module cur;
      size_t ind = 0;
      if (line.front() == '%') {
         cur.type = Type::FlipFlop;
         ind++;
      } else if (line.front() == '&') {
         cur.type = Type::Conjunction;
         ind++;
      } else {
         cur.type = Type::Broadcast;
      }
      std::string label;
      for (; ind < line.size() && line[ind] != ' '; ind++) {
         label.push_back(line[ind]);
      }
      ind += 4;
      mod_from_label.insert_or_assign(label, cur);

      std::vector<std::string> mods;
      while (ind < line.size()) {
         std::string cur_label;
         for (; ind < line.size() && line[ind] != ','; ind++) {
            cur_label.push_back(line[ind]);
         }
         mods.push_back(cur_label);
         ind+=2;
      }
      //Defaults outputs to "Type::Output" if doesn't exist
      for (const auto& dest : mods) {
         if (!mod_from_label.contains(dest)) {
            mod_from_label.insert_or_assign(dest, Module{ .type = Type::Output });
         }
      }
      mod_dests.insert_or_assign(label, mods);
   }

   std::unordered_map<std::string, std::vector<Pulse>> conjunction_mem{};

   for (const auto& [sender_label, dest_labels] : mod_dests) {
      for (const auto& cur_dest_label : dest_labels) {
         if (mod_from_label.at(cur_dest_label).type == Type::Conjunction) {
            if (!conjunction_mem.contains(cur_dest_label))
               conjunction_mem.insert_or_assign(cur_dest_label, std::vector<Pulse>{Pulse{sender_label, cur_dest_label, false}});
            else
               conjunction_mem.at(cur_dest_label).push_back(Pulse{sender_label, cur_dest_label, false});
         }
      }
   }

   std::unordered_map<std::string, std::vector<size_t>> presses_till_on{};
   for (const auto& [ label, _ ] : mod_from_label) {
      presses_till_on[label] = std::vector<size_t>{};
   }

   //get the connected element from `rx`

   std::string leads_to_rx{};
   for (const auto& [ label, dests ] : mod_dests) {
      if (std::any_of(dests.cbegin(), dests.cend(), [](const auto& str) { return str == "rx"; })) {
         leads_to_rx = label;
         break;
      }
   }
   std::vector<std::string> leads_to_pre_last{};
   for (const auto& [ label, dests ] : mod_dests) {
      if (std::any_of(dests.cbegin(), dests.cend(), [&](const auto& str) { return str == leads_to_rx; })) {
         leads_to_pre_last.push_back(label);
      }
   }

   std::deque<Pulse> pulses{ Pulse{"button", "broadcaster", false} };
   for (uint64_t i = 1; true; i++, pulses.push_back(Pulse{"button", "broadcaster", false})) {
      while (pulses.size() > 0) {
         Pulse cur_pulse = pulses.front();
         pulses.pop_front();

         //module gets the pulse, and send other pulses
         Module& cur_mod = mod_from_label.at(cur_pulse.dest);
         switch (cur_mod.type) {
            case Type::Button: {
               /* pulses.push_back({{"button", "broadcaster"}, false}); */
            } break;
            case Type::Broadcast: {
               for (const auto& dest : mod_dests.at(cur_pulse.dest))
                  pulses.push_back(Pulse{cur_pulse.dest, dest, cur_pulse.is_high});
            } break;
            case Type::Conjunction: {
               std::vector<Pulse>& prev_pulses = conjunction_mem.at(cur_pulse.dest);
               for (auto& p : prev_pulses) {
                  if (p.sender == cur_pulse.sender) {
                     p.is_high = cur_pulse.is_high;
                     break;
                  }
               }

               /* prev_pulses.front().second = cur_pulse.second; */

               bool pulse_to_send = !std::ranges::all_of(prev_pulses, [](const Pulse& p) { return p.is_high; });

               if (pulse_to_send) {
                  presses_till_on[cur_pulse.dest].push_back(i);
               }

               for (const auto& dest : mod_dests.at(cur_pulse.dest))
                  pulses.push_back(Pulse{ cur_pulse.dest, dest, pulse_to_send });
            } break;
            case Type::FlipFlop: {
               if (cur_pulse.is_high == false) {
                  cur_mod.is_high = !cur_mod.is_high;
                  for (const auto& dest : mod_dests.at(cur_pulse.dest))
                     pulses.push_back(Pulse{ cur_pulse.dest, dest, cur_mod.is_high });
               }
            } break;
            case Type::Output: {
               if (cur_pulse.is_high == false) {
                  return i; //Not gonna happen, return is 238'420'328'103'151 lol
               }
            } break;
         }
      }

      if (std::all_of(leads_to_pre_last.cbegin(), leads_to_pre_last.cend(), [&](const std::string label) {
         return presses_till_on.at(label).size() != 0;
      })) {
         break;
      }
   }

   std::vector<size_t> ret{};
   for (const auto& label : leads_to_pre_last) {
      ret.push_back(presses_till_on[label][0]);
   }

   return std::ranges::fold_right(ret, 1, [](size_t cur, size_t acc) { return std::lcm(cur, acc); });
}
