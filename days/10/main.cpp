#include "aoc.hpp"

#include <fstream>
/* #include <set> */
#include <unordered_set>
#include <string_view>
#include <functional>
#include <ranges>

size_t task_1(const std::string_view file);
size_t task_2(const std::string_view file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::string_view{file});
   size_t task_2_ret = task_2(std::string_view{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

class CCoord {
public:
   int32_t x;
   int32_t y;
   CCoord(int x = 0, int y = 0)
      : x(x), y(y)
   { }

   bool hasUp() const { return y > 0; }
   bool hasDown(int32_t nb_lines) const { return y < nb_lines - 1; }
   bool hasLeft() const { return x > 0; }
   bool hasRight(int32_t line_len) const { return x < line_len - 1; }
   CCoord Up() const { return CCoord(x, y-1); }
   CCoord Down() const { return CCoord(x, y+1); }
   CCoord Left() const { return CCoord(x-1, y); }
   CCoord Right() const { return CCoord(x+1, y); }

   char GetChar(const std::string_view sv, const int line_len) const {
      return sv.at((line_len+1) * y + x);
   }
   char GetChar(const std::vector<std::string>& vec) const {
      return vec.at(y).at(x);
   }

   friend bool operator==(const CCoord& a, const CCoord& b) {
      return a.x == b.x && a.y == b.y;
   }
};

namespace std {
template<>
struct hash<CCoord> {
   size_t operator()(const CCoord& i) const noexcept {
      return hash<size_t>()((size_t(i.x) << 32) + i.y);
   }
};
}

/* class Pipe { */
/* public: */
/*    /1* enum class PipeSeg { *1/ */
      
/*    /1* }; *1/ */
/*    std::set<Coord> segments; */
/*    const std::string_view map; */
/*    Pipe(Coord co, const std::string_view sv) */
/*       : segments(std::set<Coord>{co}), */
/*         map(sv) */
/*    { */
/*       size_t line_len = sv.find('\n') + 1; */
/*       size_t nb_lines = sv.size() / line_len; */

/*       const auto find_neighbours = [&](const Coord in) -> void { */
/*          if (in.first > 0) { */
/*             char c = map[(line_len-1) * in.first + in.second]; */
/*             if (c == '|' || c == 'F' || c == '7') { */
/*                segments.insert(Coord(in.first - 1, in.second)); */
/*             } */
/*          } */
/*          if (size_t(in.first) < nb_lines - 1) { */
/*             char c = map[(line_len+1) * in.first + in.second]; */
/*             if (c == '|' || c == 'J' || c == 'L') { */
/*                segments.insert(Coord(in.first + 1, in.second)); */
/*             } */
/*          } */
/*          if (size_t(in.second) != line_len - 1) { */
/*             char c = map[line_len * in.first + in.second + 1]; */
/*             if (c == '-' || c == 'J' || c == '7') { */
/*                segments.insert(Coord(in.first, in.second + 1)); */
/*             } */
/*          } */
/*          if (size_t(in.second) != 0) { */
/*             char c = map[line_len * in.first + in.second - 1]; */
/*             if (c == '-' || c == 'F' || c == 'L') { */
/*                segments.insert(Coord(in.first-1, in.second - 1)); */
/*             } */
/*          } */
/*       }; */

/*    } */
/* }; */

/* class Pipe { */
/* public: */
/*    /1* enum class PipeSeg { *1/ */
      
/*    /1* }; *1/ */
/*    std::set<Coord> seen; */
/*    const std::string_view map; */
/*    Pipe(Coord co, const std::string_view sv) */
/*       : segments(std::set<Coord>{co}), */
/*         map(sv) */
/*    { */
/*       size_t line_len = sv.find('\n') + 1; */
/*       size_t nb_lines = sv.size() / line_len; */


/*    } */
/* }; */


size_t task_1(const std::string_view sv) {
   size_t line_len = sv.find('\n');
   size_t nb_lines = sv.size() / (line_len+1);

   /* std::cout << line_len << " " << nb_lines << "\n"; */

   CCoord start{};
   for (uint i = 0; i < sv.size(); i++) {
      if (sv[i] == 'S') {
         start.x = i / (line_len+1);
         start.y = i % (line_len+1);
         break;
      }
   }

   /* std::cout << "start: " << start.first << ":" << start.second << "\n"; */

   std::unordered_set<CCoord> seen{};

   const auto co_to_sv_ind = [&](const CCoord in) -> size_t {
      return in.x * (line_len+1) + in.y;
   };

   const auto find_neighbours = [&](const CCoord in) -> std::vector<CCoord> {
      const char seg = sv[co_to_sv_ind(in)];
      std::vector<CCoord> segments{};
      if ((in.x > 0) && (seg == '|' || seg == 'L' || seg == 'J' || seg == 'S')) {
         size_t ind = co_to_sv_ind(in) - line_len - 1;
         char c = sv[ind];
         if (c == '|' || c == 'F' || c == '7') {
            /* std::cout << "Seen up at ind " << ind << "\n"; */
            CCoord co = CCoord(in.x - 1, in.y);
            if (!seen.contains(co)) segments.push_back(co);
         }
      }
      if ((size_t(in.x) < nb_lines - 1) && (seg == '|' || seg == '7' || seg == 'F' || seg == 'S')) {
         size_t ind = co_to_sv_ind(in) + line_len + 1;
         char c = sv[ind];
         if (c == '|' || c == 'J' || c == 'L') {
            /* std::cout << "Seen down at ind " << co_to_sv_ind(in) + line_len + 1 << "\n"; */
            CCoord co = CCoord(in.x + 1, in.y);
            if (!seen.contains(co)) segments.push_back(co);
         }
      }
      if ((size_t(in.y) > 0) && (seg == '-' || seg == 'J' || seg == '7' || seg == 'S')) {
         size_t ind = co_to_sv_ind(in) - 1;
         char c = sv[ind];
         if (c == '-' || c == 'L' || c == 'F') {
            /* std::cout << "Seen left at ind " << ind << "\n"; */
            CCoord co = CCoord(in.x, in.y - 1);
            if (!seen.contains(co)) segments.push_back(co);
         }
      }
      if ((size_t(in.y) < line_len - 1) && (seg == '-' || seg == 'L' || seg == 'F' || seg == 'S')) {
         size_t ind = co_to_sv_ind(in) + 1;
         char c = sv[ind];
         if (c == '-' || c == 'J' || c == '7') {
            /* std::cout << "Seen right at ind " << ind << "\n"; */
            CCoord co = CCoord(in.x, in.y + 1);
            if (!seen.contains(co)) segments.push_back(co);
         }
      }
      seen.insert(in);
      /* std::cout << "segs: "; */
      /* for (const auto& e : segments) { */
      /*    std::cout << e.first << ":" << e.second << " "; */
      /* } */
      /* std::cout << "\n"; */
      return segments;
   };


   std::vector<CCoord> cur = find_neighbours(start);
   size_t sum = 0;
   while (true) {
      std::vector vec1 = find_neighbours(cur[0]);
      std::vector vec2 = find_neighbours(cur[1]);
      if (vec1.size() == 0 || vec2.size() == 0) {
         return sum + 1;
      }
      cur[0] = vec1[0];
      cur[1] = vec2[0];
      sum++;
   }
}

inline bool can_go_through_vertical(char in) {
   return in == '|' || in == 'L' || in == 'F' || in == 'J' || in == '7';
}
inline bool can_go_through_horizontal(char in) {
   return in == '-' || in == 'L' || in == 'F' || in == 'J' || in == '7';
}

/* size_t task_2(const std::string_view sv) { */
/*    size_t line_len = sv.find('\n'); */
/*    size_t nb_lines = sv.size() / (line_len+1); */

/*    /1* std::cout << line_len << " " << nb_lines << "\n"; *1/ */

/*    Coord start{}; */
/*    for (uint i = 0; i < sv.size(); i++) { */
/*       if (sv[i] == 'S') { */
/*          start.first = i / (line_len+1); */
/*          start.second = i % (line_len+1); */
/*          break; */
/*       } */
/*    } */

/*    /1* std::cout << "start: " << start.first << ":" << start.second << "\n"; *1/ */

/*    std::set<Coord> seen{}; */

/*    const auto co_to_sv_ind = [&](const Coord in) -> size_t { */
/*       return in.first * (line_len+1) + in.second; */
/*    }; */

/*    const auto find_neighbours = [&](const Coord in) -> std::vector<Coord> { */
/*       const char seg = sv[co_to_sv_ind(in)]; */
/*       std::vector<Coord> segments{}; */
/*       if ((in.first > 0) && (seg == '|' || seg == 'L' || seg == 'J' || seg == 'S')) { */
/*          size_t ind = co_to_sv_ind(in) - line_len - 1; */
/*          char c = sv[ind]; */
/*          if (c == '|' || c == 'F' || c == '7') { */
/*             /1* std::cout << "Seen up at ind " << ind << "\n"; *1/ */
/*             Coord co = Coord(in.first - 1, in.second); */
/*             if (!seen.contains(co)) segments.push_back(co); */
/*          } */
/*       } */
/*       if ((size_t(in.first) < nb_lines - 1) && (seg == '|' || seg == '7' || seg == 'F' || seg == 'S')) { */
/*          size_t ind = co_to_sv_ind(in) + line_len + 1; */
/*          char c = sv[ind]; */
/*          if (c == '|' || c == 'J' || c == 'L') { */
/*             /1* std::cout << "Seen down at ind " << co_to_sv_ind(in) + line_len + 1 << "\n"; *1/ */
/*             Coord co = Coord(in.first + 1, in.second); */
/*             if (!seen.contains(co)) segments.push_back(co); */
/*          } */
/*       } */
/*       if ((size_t(in.second) > 0) && (seg == '-' || seg == 'J' || seg == '7' || seg == 'S')) { */
/*          size_t ind = co_to_sv_ind(in) - 1; */
/*          char c = sv[ind]; */
/*          if (c == '-' || c == 'L' || c == 'F') { */
/*             /1* std::cout << "Seen left at ind " << ind << "\n"; *1/ */
/*             Coord co = Coord(in.first, in.second - 1); */
/*             if (!seen.contains(co)) segments.push_back(co); */
/*          } */
/*       } */
/*       if ((size_t(in.second) < line_len - 1) && (seg == '-' || seg == 'L' || seg == 'F' || seg == 'S')) { */
/*          size_t ind = co_to_sv_ind(in) + 1; */
/*          char c = sv[ind]; */
/*          if (c == '-' || c == 'J' || c == '7') { */
/*             /1* std::cout << "Seen right at ind " << ind << "\n"; *1/ */
/*             Coord co = Coord(in.first, in.second + 1); */
/*             if (!seen.contains(co)) segments.push_back(co); */
/*          } */
/*       } */
/*       seen.insert(in); */
/*       /1* std::cout << "segs: "; *1/ */
/*       /1* for (const auto& e : segments) { *1/ */
/*       /1*    std::cout << e.first << ":" << e.second << " "; *1/ */
/*       /1* } *1/ */
/*       /1* std::cout << "\n"; *1/ */
/*       return segments; */
/*    }; */


/*    std::vector<Coord> cur = find_neighbours(start); */
/*    while (true) { */
/*       std::vector vec1 = find_neighbours(cur[0]); */
/*       std::vector vec2 = find_neighbours(cur[1]); */
/*       if (vec1.size() == 0 || vec2.size() == 0) { */
/*          break; */
/*       } */
/*       cur[0] = vec1[0]; */
/*       cur[1] = vec2[0]; */
/*    } */

/*    std::set<Coord> connected_to_outside{}; */

/*    std::function<bool(Coord, int)> is_open_right; */
/*    std::function<bool(Coord, int)> is_open_left; */
/*    std::function<bool(Coord, int)> is_open_down; */

/*    const auto is_open_up = [&](Coord in, int inside_pipe = 0) -> bool { */
/*       /1* int inside_pipe = 0; //0 means no, 1 means yes and exit to the left, 2 means yes and exit to the right *1/ */
/*       //3 means that it successfully exited, so can look left or right */
/*       for (; in.first >= 0; in.first--) { */
/*          char c = sv.at(co_to_sv_ind(in)); */
/*          if (inside_pipe == 1) { */
/*             if (c == '|') continue; */
/*             else if (c == 'F') { */
/*                inside_pipe = 3; */
/*                if (is_open_left(in, 1)) return true; */
/*                else if (is_open_right(in, 1)) return true; */
/*                continue; */
/*             } else if (c == '7') return false; */
/*          } else if (inside_pipe == 2) { */
/*             if (c == '|') continue; */
/*             else if (c == '7') { */
/*                inside_pipe = 3; */
/*                if (is_open_left(in, 1)) return true; */
/*                else if (is_open_right(in, 1)) return true; */
/*                continue; */
/*             } else if (c == 'F') return false; */
/*          } else if (inside_pipe == 3) { */
/*             if (is_open_left(in, 2)) return true; */
/*             else if (is_open_right(in, 2)) return true; */
/*             inside_pipe = 0; */
/*          } */
/*          if (connected_to_outside.contains(in)) return true; */
/*          if (seen.contains(in)) { */
/*             if (c == 'L') { */
/*                inside_pipe = 1; */
/*                continue; */
/*             } */
/*             else if (c == 'J') { */
/*                inside_pipe = 2; */
/*                continue; */
/*             } */
/*             return false; */
/*          } */
/*       } */
/*       return true; */
/*    }; */
/*    is_open_down = [&](Coord in, int inside_pipe = 0) -> bool { */
/*       /1* int inside_pipe = 0; //0 means no, 1 means yes and exit to the left, 2 means yes and exit to the right *1/ */
/*       for (; in.first < int(nb_lines); in.first++) { */
/*          char c = sv.at(co_to_sv_ind(in)); */
/*          if (inside_pipe == 1) { */
/*             if (c == '|') continue; */
/*             else if (c == 'L') { */
/*                inside_pipe = 3; */
/*                if (is_open_left(in, 2)) return true; */
/*                else if (is_open_right(in, 2)) return true; */
/*                continue; */
/*             } else if (c == 'J') return false; */
/*          } else if (inside_pipe == 2) { */
/*             if (c == '|') continue; */
/*             else if (c == 'J') { */
/*                inside_pipe = 3; */
/*                if (is_open_left(in, 2)) return true; */
/*                else if (is_open_right(in, 2)) return true; */
/*                continue; */
/*             } else if (c == 'L') return false; */
/*          } else if (inside_pipe == 3) { */
/*             if (is_open_left(in, 1)) return true; */
/*             else if (is_open_right(in, 1)) return true; */
/*             inside_pipe = 0; */
/*          } */
/*          if (connected_to_outside.contains(in)) return true; */
/*          if (seen.contains(in)) { */
/*             if (c == 'F') { */
/*                inside_pipe = 1; */
/*                continue; */
/*             } */
/*             else if (c == '7') { */
/*                inside_pipe = 2; */
/*                continue; */
/*             } */
/*             return false; */
/*          } */
/*       } */
/*       return true; */
/*    }; */
/*    is_open_right = [&](Coord in, int inside_pipe = 0) -> bool { */
/*       /1* int inside_pipe = 0; //0 means no, 1 means yes and exit up, 2 means yes and exit to the down *1/ */
/*       for (; in.second < int(line_len); in.second++) { */
/*          char c = sv.at(co_to_sv_ind(in)); */
/*          if (inside_pipe == 1) { */
/*             if (c == '-') continue; */
/*             else if (c == '7') { */
/*                inside_pipe = 3; */
/*                /1* if (is_open_up(in, 1)) return true; *1/ */
/*                /1* else if (is_open_down(in, 1)) return true; *1/ */
/*                continue; */
/*             } else if (c == 'J') return false; */
/*          } else if (inside_pipe == 2) { */
/*             if (c == '-') continue; */
/*             else if (c == 'J') { */
/*                inside_pipe = 3; */
/*                if (is_open_up(in, 1)) return true; */
/*                else if (is_open_down(in, 1)) return true; */
/*                continue; */
/*             } else if (c == '7') return false; */
/*          } else if (inside_pipe == 3) { */
/*             if (is_open_up(in, 2)) return true; */
/*             else if (is_open_down(in, 2)) return true; */
/*             inside_pipe = 0; */
/*          } */
/*          if (connected_to_outside.contains(in)) return true; */
/*          if (seen.contains(in)) { */
/*             if (c == 'F') { */
/*                inside_pipe = 1; */
/*                continue; */
/*             } */
/*             else if (c == 'L') { */
/*                inside_pipe = 2; */
/*                continue; */
/*             } */
/*             return false; */
/*          } */
/*       } */
/*       return true; */
/*    }; */
/*    is_open_left = [&](Coord in, int inside_pipe = 0) -> bool { */
/*       /1* int inside_pipe = 0; //0 means no, 1 means yes and exit up, 2 means yes and exit down *1/ */
/*       for (; in.second >= 0; in.second--) { */
/*          char c = sv.at(co_to_sv_ind(in)); */
/*          if (inside_pipe == 1) { */
/*             if (c == '-') continue; */
/*             else if (c == 'F') { */
/*                inside_pipe = 3; */
/*                /1* if (is_open_up(in, 2)) return true; *1/ */
/*                /1* else if (is_open_down(in, 2)) return true; *1/ */
/*                continue; */
/*             } else if (c == 'L') return false; */
/*          } else if (inside_pipe == 2) { */
/*             if (c == '-') continue; */
/*             else if (c == 'L') { */
/*                inside_pipe = 3; */
/*                /1* if (is_open_up(in, 2)) return true; *1/ */
/*                /1* else if (is_open_down(in, 2)) return true; *1/ */
/*                continue; */
/*             } else if (c == 'F') return false; */
/*          } else if (inside_pipe == 3) { */
/*             /1* if (is_open_up(in, 1)) return true; *1/ */
/*             /1* else if (is_open_down(in, 1)) return true; *1/ */
/*             inside_pipe = 0; */
/*          } */
/*          if (connected_to_outside.contains(in)) return true; */
/*          if (seen.contains(in)) { */
/*             if (c == '7') { */
/*                inside_pipe = 1; */
/*                continue; */
/*             } */
/*             else if (c == 'J') { */
/*                inside_pipe = 2; */
/*                continue; */
/*             } */
/*             return false; */
/*          } */
/*       } */
/*       return true; */
/*    }; */

/*    //After first pass, can optimise perf by searching withing the already seen */
/*    while(true) { */
/*       std::vector<Coord> current_connected_to_outside{}; */
/*       for (uint i = 0; i < nb_lines; i++) { */
/*          for (uint j = 0; j < line_len; j++) { */
/*             Coord co = Coord(i, j); */
/*             if (seen.contains(co) || connected_to_outside.contains(co)) { */
/*                continue; */
/*             } */
/*             if (is_open_up(co, 0) || is_open_right(co, 0) || is_open_down(co, 0) || is_open_left(co, 0)) { */
/*                current_connected_to_outside.push_back(co); */
/*                connected_to_outside.insert(co); */
/*             } */
/*          } */
/*       } */
/*       if (current_connected_to_outside.size() == 0) { */
/*          break; */
/*       } */
/*    } */

/*    return (sv.size() - nb_lines) - seen.size() - connected_to_outside.size(); */
/* } */

size_t task_2(const std::string_view sv) {
   size_t line_len = sv.find('\n');
   size_t nb_lines = sv.size() / (line_len+1);

   /* std::cout << line_len << " " << nb_lines << "\n"; */

   CCoord start{};
   for (uint i = 0; i < sv.size(); i++) {
      if (sv[i] == 'S') {
         start.x = i % (line_len+1);
         start.y = i / (line_len+1);
         break;
      }
   }
   /* std::cout << "start: " << start.x << " " << start.y << "\n"; */

   //find out what start is

   std::unordered_set<CCoord> pipe{};

   const auto co_to_sv_ind = [&](const CCoord in) -> size_t {
      return in.y * (line_len+1) + in.x;
   };

   const auto find_neighbours = [&](const CCoord in) -> std::vector<CCoord> {
      const char seg = sv[co_to_sv_ind(in)];
      /* std::cout << "Cur seg: '" << seg << "'\n"; */
      std::vector<CCoord> segments{};
      if (in.hasUp() && (seg == '|' || seg == 'L' || seg == 'J' || seg == 'S')) {
         CCoord co = in.Up();
         char c = co.GetChar(sv, line_len);
         if (c == '|' || c == 'F' || c == '7') {
            /* std::cout << "Seen up at ind " << "\n"; */
            if (!pipe.contains(co)) segments.push_back(co);
         }
      }
      if (in.hasDown(nb_lines) && (seg == '|' || seg == '7' || seg == 'F' || seg == 'S')) {
         CCoord co = in.Down();
         char c = co.GetChar(sv, line_len);
         if (c == '|' || c == 'J' || c == 'L') {
            /* std::cout << "Seen down at ind " << "\n"; */
            if (!pipe.contains(co)) segments.push_back(co);
         }
      }
      if (in.hasLeft() && (seg == '-' || seg == 'J' || seg == '7' || seg == 'S')) {
         CCoord co = in.Left();
         char c = co.GetChar(sv, line_len);
         if (c == '-' || c == 'L' || c == 'F') {
            /* std::cout << "Seen left at ind " << "\n"; */
            if (!pipe.contains(co)) segments.push_back(co);
         }
      }
      if (in.hasRight(line_len) && (seg == '-' || seg == 'L' || seg == 'F' || seg == 'S')) {
         CCoord co = in.Right();
         char c = co.GetChar(sv, line_len);
         if (c == '-' || c == 'J' || c == '7') {
            /* std::cout << "Seen right at ind " << "\n"; */
            CCoord co = in.Right();
            if (!pipe.contains(co)) segments.push_back(co);
         }
      }
      pipe.insert(in);

      /* std::cout << "segs: "; */
      /* for (const auto& e : segments) { */
      /*    std::cout << e.first << ":" << e.second << " "; */
      /* } */
      /* std::cout << "\n"; */
      return segments;
   };

   std::vector<CCoord> cur = find_neighbours(start);
   while (true) {
      std::vector vec1 = find_neighbours(cur[0]);
      std::vector vec2 = find_neighbours(cur[1]);
      if (vec1.size() == 0 || vec2.size() == 0) {
         break;
      }
      cur[0] = vec1[0];
      cur[1] = vec2[0];
   }

   std::vector<std::string> big_map(nb_lines * 3);
   size_t cur_line = 0;
   for (const auto line : std::views::split(sv, '\n')) {
      for (uint i = 0; i < line.size(); i++) {
         CCoord co(i, cur_line);
         if (pipe.contains(co)) {
            switch (co.GetChar(sv, line_len)) {
               case '|': {
                  big_map[cur_line * 3] +=     ".#.";
                  big_map[cur_line * 3 + 1] += ".#.";
                  big_map[cur_line * 3 + 2] += ".#.";
               } break;
               case '-': {
                  big_map[cur_line * 3] +=     "...";
                  big_map[cur_line * 3 + 1] += "###";
                  big_map[cur_line * 3 + 2] += "...";
               } break;
               case 'L': {
                  big_map[cur_line * 3] +=     ".#.";
                  big_map[cur_line * 3 + 1] += ".##";
                  big_map[cur_line * 3 + 2] += "...";
               } break;
               case 'J': {
                  big_map[cur_line * 3] +=     ".#.";
                  big_map[cur_line * 3 + 1] += "##.";
                  big_map[cur_line * 3 + 2] += "...";
               } break;
               case '7': {
                  big_map[cur_line * 3] +=     "...";
                  big_map[cur_line * 3 + 1] += "##.";
                  big_map[cur_line * 3 + 2] += ".#.";
               } break;
               case 'F': {
                  big_map[cur_line * 3] +=     "...";
                  big_map[cur_line * 3 + 1] += ".##";
                  big_map[cur_line * 3 + 2] += ".#.";
               } break;
               default: {
                  /* std::cout <<"wut\n"; */
                  big_map[cur_line * 3] +=     "###";
                  big_map[cur_line * 3 + 1] += "###";
                  big_map[cur_line * 3 + 2] += "###";
               }
            }
         } else {
            big_map[cur_line * 3] +=     "...";
            big_map[cur_line * 3 + 1] += "...";
            big_map[cur_line * 3 + 2] += "...";
         }
      }
      cur_line++;
      if (cur_line == nb_lines) break;
   }

   std::function<void(const CCoord&)> fill;
   fill = [&](const CCoord& in) -> void {
      char c = in.GetChar(big_map);
      if (c != '.') {
         return;
      }
      
      big_map[in.y][in.x] = 'o';
      
      if (in.hasUp()) {
         fill(in.Up());
      }
      if (in.hasRight(line_len * 3)) {
         fill(in.Right());
      }
      if (in.hasDown(nb_lines * 3)) {
         fill(in.Down());
      }
      if (in.hasLeft()) {
         fill(in.Left());
      }
   };


   /* fill(Coord(line_len * 3 - 1, nb_lines * 3 - 1)); */
   fill(CCoord(0, 0));

   /* for (auto vec : big_map) { */
   /*    for (auto e : vec) { */
   /*       std::cout << e << " "; */
   /*    } */
   /*    std::cout << "\n"; */
   /* } */

   size_t sum = 0;
   for (uint x = 0; x < line_len * 3; x += 3) {
      for (uint y = 0; y < nb_lines * 3; y += 3) {
         if (big_map[y][x] == '.' &&  big_map[y][x+1] == '.' &&  big_map[y][x+2] == '.'
         &&  big_map[y+1][x] == '.' &&  big_map[y+1][x+1] == '.' &&  big_map[y+1][x+2] == '.'
         &&  big_map[y+2][x] == '.' &&  big_map[y+2][x+1] == '.' &&  big_map[y+2][x+2] == '.') {
            sum++;
         }
      }
   }




   return sum;
}
