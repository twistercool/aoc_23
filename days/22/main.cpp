#include "aoc.hpp"

#include <fstream>
#include <string_view>
#include <ranges>
#include <charconv>
#include <cassert>
#include <unordered_set>
#include <algorithm>

size_t task_1(std::string_view file);
size_t task_2(std::string_view file);
void test();

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   std::string file(ss.str());
   if (file.size() > 0)
      file.erase(file.end()-1);

   test();

   Timer t;
   size_t task_1_ret = task_1(std::string_view{file});
   size_t task_2_ret = task_2(std::string_view{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

struct Brick {
   Vector3<int64_t> start;
   Vector3<int64_t> end;

   Brick(const std::string_view sv) {
      auto coord_it = std::views::split(sv, '~').cbegin();
      auto nb_it = std::views::split(std::string_view{*coord_it}, ',').cbegin();

      std::from_chars((*nb_it).cbegin(), (*nb_it++).cend(), start.x);
      std::from_chars((*nb_it).cbegin(), (*nb_it++).cend(), start.y);
      std::from_chars((*nb_it).cbegin(), (*nb_it).cend(), start.z);

      coord_it++;
      nb_it = std::views::split(std::string_view{*coord_it}, ',').cbegin();

      std::from_chars((*nb_it).cbegin(), (*nb_it++).cend(), end.x);
      std::from_chars((*nb_it).cbegin(), (*nb_it++).cend(), end.y);
      std::from_chars((*nb_it).cbegin(), (*nb_it).cend(), end.z);
   }

   Brick(const Vector3<int64_t> start, const Vector3<int64_t> end) : start(start), end(end) {}

   bool is_point_above(const Vector3<int64_t>& pos) const {
      if (std::max(start.z, end.z) <= pos.z) return false;

      return (start.y == end.y && start.y == pos.y && pos.x >= std::min(start.x, end.x) && pos.x <= std::max(start.x, end.x))
         ||  (start.x == end.x && start.x == pos.x && pos.y >= std::min(start.y, end.y) && pos.y <= std::max(start.y, end.y));
   }

   //assumes bricks are not diagonal
   bool is_above(const Brick& in) const {
      //TODO make faster
      
      if (in.start == in.end) return is_point_above(in.start);

      const Vector3<int64_t> diff = (in.start.x != in.end.x) ? Vector3<int64_t>{(in.start.x < in.end.x) ? 1 : -1, 0, 0} :
         (in.start.y != in.end.y) ? Vector3<int64_t>{0, (in.start.y < in.end.y) ? 1 : -1, 0} :
         Vector3<int64_t>{0, 0, (in.start.z < in.end.z) ? 1 : -1};

      for (Vector3<int64_t> cur = in.start; cur != in.end + diff; cur += diff) {
         if (is_point_above(cur)) {
            return true;
         }
      }
      return false;
   }

   Brick get_lowered(const std::vector<Brick>& bricks) {
      Brick highest_lower_brick{ {0, 0, 0}, {0, 0, 0} };

      for (int i = bricks.size() - 1; i >= 0; i--) {
         const Brick& cur = bricks[i];
         if (is_above(cur) && std::max(cur.start.z, cur.end.z) > std::max(highest_lower_brick.start.z, highest_lower_brick.end.z)) {
            highest_lower_brick = cur;
         }
      }

      const auto len_z = std::max(start.z, end.z) - std::min(start.z, end.z) + 1;
      return Brick{ {start.x, start.y, std::max(highest_lower_brick.start.z, highest_lower_brick.end.z) + 1}, { end.x, end.y, std::max(highest_lower_brick.start.z, highest_lower_brick.end.z) + len_z} };
   }

   std::vector<Brick> get_right_below(const std::vector<Brick>& bricks) const {
      std::vector<Brick> ret{};
      for (const auto& brick : bricks) {
         if (std::max(brick.start.z, brick.end.z) + 1 == std::min(start.z, end.z) && is_above(brick)) {
            ret.push_back(brick);
         }
      }
      return ret;
   }

   std::vector<Brick> get_right_above(const std::vector<Brick>& bricks) const {
      std::vector<Brick> ret{};
      for (const auto& brick : bricks) {
         if (std::min(brick.start.z, brick.end.z) == std::max(start.z, end.z) + 1 && brick.is_above(*this)) {
            ret.push_back(brick);
         }
      }
      return ret;
   }

   void get_dependant_bricks(const std::vector<Brick> bricks, std::unordered_set<Brick>& dependant_bricks) const;

   bool is_safe_to_desintegrate(const std::vector<Brick>& all_bricks) const {
      std::vector<Brick> bricks_right_above = get_right_above(all_bricks);

      //If it has no bricks above, can desintegrate
      if (bricks_right_above.size() == 0) {
         return true;
      }

      //If it has bricks on top, can desintegrate if all of the bricks on top are all supported by at least one extra brick
      for (const auto& brick : bricks_right_above) {
         std::vector<Brick> bricks_below_brick_on_top = brick.get_right_below(all_bricks);
         if (bricks_below_brick_on_top.size() <= 1) {
            return false;
         }
      }

      return true;
   }

   friend bool operator==(const Brick& a, const Brick& b) = default;

   friend std::ostream& operator<<(std::ostream& os, const Brick in) {
      return os << in.start << " -> " << in.end;
   }
};

namespace std {
template<>
struct std::hash<Brick> {
   size_t operator()(const Brick& in) const {
      return std::hash<Vector3<int64_t>>{}(std::hash<decltype(in.start)>{}(in.start) ^ ~std::hash<decltype(in.end)>{}(in.end));
   }
};
}

void Brick::get_dependant_bricks(const std::vector<Brick> bricks, std::unordered_set<Brick>& dependant_bricks) const {
   const std::vector<Brick> bricks_on_top = get_right_above(bricks);
   std::vector<Brick> current_dependant_bricks{};

   for (const auto& top_brick : bricks_on_top) {
      std::vector<Brick> bricks_below = top_brick.get_right_below(bricks);
      if (bricks_below.size() <= 1) {
         if (!dependant_bricks.contains(top_brick)) {
            current_dependant_bricks.push_back(top_brick);
            dependant_bricks.insert(top_brick);
         }
      } else {
         const bool all_dependant = std::ranges::all_of(bricks_below, [&](const Brick& in) { return dependant_bricks.contains(in); });
         if (all_dependant && !dependant_bricks.contains(top_brick)) {
               current_dependant_bricks.push_back(top_brick);
               dependant_bricks.insert(top_brick);
         }
      }

   }
   
   for (const auto& brick : current_dependant_bricks) {
      brick.get_dependant_bricks(bricks, dependant_bricks);
   }
}

void test() {
   Brick a { {1, 0, 1}, {1, 2, 1} };
   Brick b { {0, 0, 2}, {2, 0, 2} };
   Brick c { {0, 2, 3}, {2, 2, 3} };
   Brick d { {0, 0, 4}, {0, 2, 4} };
   Brick e { {2, 0, 5}, {2, 2, 5} };
   Brick f { {0, 1, 6}, {2, 1, 6} };
   Brick g { {1, 1, 8}, {1, 1, 9} };

   assert(!a.is_above(b));
   assert(b.is_above(a));

   assert(b.get_lowered(std::vector<Brick>{a}) == b);

   /* assert(!a.is_safe_to_desintegrate(std::vector<Brick>{a, b})); */
   /* assert(b.is_safe_to_desintegrate(std::vector<Brick>{a, b})); */
}

size_t task_1(std::string_view file) {
   std::vector<Brick> bricks{};
   for (const auto& line_sp : std::views::split(file, '\n')) {
      const std::string_view line = std::string_view{line_sp.begin(), line_sp.end()};
      bricks.emplace_back(line);
   }

   std::sort(bricks.begin(), bricks.end(), [](const Brick& a, const Brick& b) -> bool {
      return std::max(a.start.z, a.end.z) < std::max(b.start.z, b.end.z);
   });

   std::vector<Brick> stacked_bricks{};
   for (uint i = 0; i < bricks.size(); i++) {
      const Brick lowered = bricks[i].get_lowered(stacked_bricks);
      stacked_bricks.push_back(lowered);
   }

   /* for (const auto& a : stacked_bricks) { */
   /*    std::cout << a << "\n"; */
   /* } */

   size_t sum = 0;
   for (const auto& brick : stacked_bricks) {
      if (brick.is_safe_to_desintegrate(stacked_bricks)) {
         /* std::cout << brick << " is safe to remove\n\n"; */
         sum++;
      } else {
         /* std::cout << brick << " is NOT safe to remove\n\n"; */
      }
   }

   return sum;
}

size_t task_2(std::string_view file) {
   std::vector<Brick> bricks{};
   for (const auto& line_sp : std::views::split(file, '\n')) {
      const std::string_view line = std::string_view{line_sp.cbegin(), line_sp.cend()};
      bricks.emplace_back(line);
   }

   std::sort(bricks.begin(), bricks.end(), [](const Brick& a, const Brick& b) -> bool {
      return std::max(a.start.z, a.end.z) < std::max(b.start.z, b.end.z);
   });

   std::vector<Brick> stacked_bricks{};
   for (uint i = 0; i < bricks.size(); i++) {
      const Brick lowered = bricks[i].get_lowered(stacked_bricks);
      stacked_bricks.push_back(lowered);
   }

   size_t sum = 0;
   for (const auto& brick : stacked_bricks) {
      std::unordered_set<Brick> fallen_bricks{brick};
      brick.get_dependant_bricks(stacked_bricks, fallen_bricks);
      sum += fallen_bricks.size() - 1;
      /* std::cout << brick << " has " << fallen_bricks.size() << " bricks that fall\n"; */

      /* for (const auto& c : fallen_bricks) { */
      /*    std::cout << "- " << c << "\n"; */
      /* } */
      /* std::cout << "\n"; */
   }

   return sum;
}
