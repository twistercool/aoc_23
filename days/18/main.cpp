#include "aoc.hpp"

#include <fstream>
#include <functional>
#include <numeric>
#include <limits>
#include <set>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout
   << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

size_t task_1(std::stringstream file) {
   std::string line;
   std::vector<std::vector<bool>> dug{};
   dug.emplace_back(false);

   Coordi32 cur{0, 0};
   for (uint i = 0; std::getline(file, line); i++) {
      Diri32 cur_dir;
      uint ind = 0;
      switch (line[ind]) {
         case 'R': cur_dir = {1, 0}; break;
         case 'L': cur_dir = {-1, 0}; break;
         case 'U': cur_dir = {0, -1}; break;
         case 'D': cur_dir = {0, 1}; break;
         default: std::cerr << "wut\n";
      }
      for (; ind < line.size() && !isdigit(line[ind]); ind++) { }
      int64_t nb = 0;
      for (; ind < line.size() && isdigit(line[ind]); ++ind) {
         nb = nb * 10 + line[ind] - '0';
      }
      /* cur_dir = {cur_dir.first * nb, cur_dir.second * nb}; */
      //Parse colour afterwards

      /* std::cout << "cur: " << cur.first << ":" << cur.second << "\n"; */
      /* std::cout << "dir: " << cur_dir.first << ":" << cur_dir.second << "\n"; */
      /* std::cout << "dir times nb: " << cur_dir.first * nb << ":" << cur_dir.second * nb << "\n"; */

      //Ensure cur and cur+dir are 
      Coordi32 next = {cur.first + nb * cur_dir.first, cur.second + nb * cur_dir.second};
      /* std::cout << "next: " << next.first << ":" << next.second << "\n\n"; */
      if (next.first >= int32_t(dug.at(0).size())) {
         for (auto& vec : dug) {
            while (int32_t(vec.size()) <= next.first) { vec.push_back(false); }
         }
      }
      while (next.second >= int32_t(dug.size())) {
         dug.push_back(std::vector<bool>(dug.at(0).size(), false));
      }
      while (next.second < 0) {
         dug.insert(dug.begin(), std::vector<bool>(dug.at(0).size(), false));
         cur.second++;
         next.second++;
      }
      if (next.first < 0) {
         int64_t nb_to_insert = -next.first;
         for (auto& vec : dug) {
            //insert_range is C++23
            for (uint i = 0; i < nb_to_insert; i++)
               vec.insert(vec.begin(), false);
         }
         cur.first += nb_to_insert;
         next.first += nb_to_insert;
      }

      Coordi32 inc = cur;
      for (; inc != next; inc = {inc.first + cur_dir.first, inc.second + cur_dir.second}) {
         dug[inc.second][inc.first] = true;
      }
      dug[inc.second][inc.first] = true;

      cur = next;
   }


   /* for (const auto& vec : dug) { */
   /*    for (const bool& b : vec) { */
   /*       std::cout << (b ? "#" : "."); */
   /*    } */
   /*    std::cout << "\n"; */
   /* } */

   std::vector<std::vector<bool>> outside(dug.size(), std::vector<bool>(dug.at(0).size(), false));

   std::function<void(const Coordi32)> fill;
   /* std::function<void(const Coord)> fill; */
   fill = [&](const Coordi32 in) {
      if (in.first < 0 || in.second < 0 || in.first >= int32_t(dug.at(0).size()) || in.second >= int32_t(dug.size())) {
         return;
      }
      const bool boo = dug.at(in.second).at(in.first) || outside.at(in.second).at(in.first);
      if (boo) return;
      outside[in.second][in.first] = true;
      fill(Coordi32{in.first-1, in.second});
      fill(Coordi32{in.first+1, in.second});
      fill(Coordi32{in.first, in.second+1});
      fill(Coordi32{in.first, in.second-1});
   };
   
   for (uint i = 0; i < dug.size(); i++) {
      fill(Coordi32{0, i});
      fill(Coordi32{dug.at(0).size()-1, i});
   }
   for (uint i = 0; i < dug.at(0).size(); i++) {
      fill(Coordi32{i, 0});
      fill(Coordi32{i, dug.size()-1});
   }

   size_t sum = std::accumulate(outside.cbegin(), outside.cend(), size_t{0}, [](size_t acc, const auto& vec) {
      return acc + std::count(vec.cbegin(), vec.cend(), false);
   });

   return sum;
}


class Line {
public:
   Coordi32 a;
   Coordi32 b;

   Line(Coordi32 a, Coordi32 b) : a(a), b(b) { }

   //todo
   std::optional<Coordi32> GetIntersection(const Line& in) const {
      return {};
   }

   //todo
   std::vector<Line> GetClosestParallel(std::vector<Line>& in) const {
      return {};
   }

   bool isHoriztonal() const { return a.second == b.second; }
   bool isVertical() const { return a.first == b.first; }

   bool isIntersectY(const int32_t in_y) const {
      if (isHoriztonal()) {
         return in_y == a.second /*or b.second*/;
      } else {
         return (in_y >= a.second && in_y <= b.second)
            || (in_y >= b.second && in_y <= a.second);
      }
   }

   friend std::ostream& operator<<(std::ostream& os, const Line& l) {
      return os << "(" << l.a.first << ":" << l.a.second << ") -> (" << l.b.first << ":" << l.b.second << ")";
   }

   int operator<=>(const Line& in) const {
      if (a != in.a) {
         return a <=> in.a;
      }
      return b <=> in.b;
   }
};

size_t task_2(std::stringstream file) {
   std::string line;
   std::vector<Line> lines{};

   size_t sum = 0;

   Coordi32 cur{0, 0};
   for (uint i = 0; std::getline(file, line); i++) {
      Diri32 cur_dir;
      uint ind = 0;
      switch (*(line.end() - 2)) {
         case '0': cur_dir = {1, 0}; break;
         case '1': cur_dir = {0, 1}; break;
         case '2': cur_dir = {-1, 0}; break;
         case '3': cur_dir = {0, -1}; break;
         default: std::cerr << "wut\n";
      }
      int64_t nb = 0;
      for (int i = 4; i >= 0; i--) {
         char c = *(line.end() - 3 - i);
         if (isalpha(c)) {
            c = tolower(c);
            c -= 'a';
            c += 10;
         } else {
            c -= '0';
         }
         nb = 16 * nb + c;
         /* std::cout << "nb: " << nb << "\n"; */
      }

      Coordi32 next = {cur.first + nb * cur_dir.first, cur.second + nb * cur_dir.second};

      lines.emplace_back(cur, next);

      cur = next;
   }


   /* for (const Line& l : lines) { */
   /*    std::cout << l.a.first << ":" << l.a.second << "->" << l.b.first << ":" << l.b.second << "\n"; */
   /* } */

   //get the topmost, leftmost corner
   //and rightmost, 
   Coordi32 cur_top_left = {INT32_MAX, INT32_MAX};
   Coordi32 cur_bottom_right = {1, 1};
   std::vector<Line> v_lines{};
   std::vector<Line> h_lines{};

   for (const auto& line : lines) {
      if (line.a < cur_top_left) cur_top_left = line.a;
      if (line.b < cur_top_left) cur_top_left = line.b;
      if (line.a > cur_bottom_right) cur_bottom_right = line.a;
      if (line.b > cur_bottom_right) cur_bottom_right = line.b;
      if (line.isVertical()) v_lines.push_back(line);
      if (line.isHoriztonal()) {
         auto swapped = Line{line.b, line.a};
         if (line.a < line.b) h_lines.push_back(line);
         else h_lines.push_back(swapped);
      }
   }

   Coordi32 cur_coord = cur_top_left;
   size_t line_size = 0;
   for (int i = cur_top_left.second; i <= cur_bottom_right.second; i++) {
      //get all vertical lines
      std::set<Line> cur_v_lines{};
      for (const auto& line : v_lines) {
         if (line.isIntersectY(i)) {
            cur_v_lines.insert(line);
         }
      }
      const std::set<Line> const_cur_v_lines = cur_v_lines;
      //get all horizontal lines
      std::set<Line> cur_h_lines{};
      for (const auto& line : h_lines) {
         if (line.isIntersectY(i)) {
            cur_h_lines.insert(line);
         }
      }
      const std::set<Line> const_cur_h_lines = cur_h_lines;
      
      int32_t x = INT32_MAX;
      for (const auto& line : cur_v_lines) {
         if (line.a.first < x) { x = line.a.first; }
      }
      for (const auto& line : cur_h_lines) {
         if (line.a.first < x) { x = line.a.first; }
         if (line.b.first < x) { x = line.b.first; }
      }

      const auto is_v_line_to_point_from_top = [&](const Coordi32& co) -> bool {
         for (const auto& line : const_cur_v_lines) {
            if (line.a == co) {
               return line.a > line.b;
            } else if (line.b == co) {
               return line.a < line.b;
            }
         }

         std::cerr << "what...\n";
         return 0;
      };

      const auto is_v_line_on_h = [&](const Coordi32& co) {
         for (const auto& line : const_cur_h_lines) {
            if (line.b == co || line.a == co) {
               return true;
            }
         }
         return false;
      };

      //until the end of the line 
      bool count_inside = false;
      bool is_from_top;
      while (!cur_v_lines.empty()) {
         //
         //If it is the beginning of a v_line
         bool continue_while = false;
         for (const auto& line : cur_v_lines) {
            if (x == line.a.first) {
               cur_v_lines.erase(line);
               line_size++;

               if (!is_v_line_on_h(Coordi32{x, i})) {
                  count_inside = !count_inside;
               }
               continue_while = true;
               break;
            }
         } if (continue_while) continue;

         //If it is the beginning of a horiztonal line
         continue_while = false;
         for (const auto& line : cur_h_lines) {
            if (x == line.a.first) {
               if (is_v_line_to_point_from_top(Coordi32{x, i}) != is_v_line_to_point_from_top(Coordi32{line.b.first, i})) {
                  count_inside = !count_inside;
               }

               line_size += line.b.first - x - 1;
               x = line.b.first;

               cur_h_lines.erase(line);

               continue_while = true;
               break;
            }
         } if (continue_while) continue;

         //find next line to the right that intersects
         int32_t next_x_horizontal = INT32_MAX;
         for (const auto& line : cur_h_lines) {
            if (x <= line.a.first) {
               if (line.a.first < next_x_horizontal) {
                  next_x_horizontal = line.a.first;
               }
            }
         }
         int32_t next_x_vertical = INT32_MAX;
         for (const auto& line : cur_v_lines) {
            if (x <= line.a.first) {
               if (line.a.first < next_x_vertical) {
                  next_x_vertical = line.a.first;
               }
            }
         }

         bool is_next_horizontal;

         if (INT32_MAX == next_x_horizontal && INT32_MAX == next_x_vertical) {
            line_size++;
            break;
         } else if (next_x_horizontal == INT32_MAX) {
            is_next_horizontal = false;
         } else if (next_x_vertical == INT32_MAX) {
            is_next_horizontal = true;
         } else {
            is_next_horizontal = next_x_horizontal < next_x_vertical;
         }

         if (is_next_horizontal) {
            x = next_x_horizontal;
         } else {
            if (count_inside) {
               line_size += next_x_vertical - x - 1;
            }
            x = next_x_vertical;
         }
      }

      sum += line_size;
      line_size = 0;
      count_inside = false;
   }

   return sum;
}
