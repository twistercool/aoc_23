#include "aoc.hpp"

#include <vector>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <cstring>
#include <ranges>
#include <string_view>

size_t task_1(const std::string_view file);
size_t task_2(const std::string_view file);

int main(int argc, const char *argv[]) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss{};
   ss << file_s.rdbuf();
   const std::string file(ss.str());
   const std::string_view f{file};

   Timer t;
   size_t task_1_ret = task_1(f);
   size_t task_2_ret = task_2(f);
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

size_t task_1(const std::string_view file) {
   uint64_t sum = 0;
   for (const std::ranges::subrange<const char*> line : std::views::split(file, '\n')) {
      if (line.size() == 0) break;
      size_t first = 0, last = line.size()-1;
      for (; first < line.size() && !isdigit(line[first]); ++first);
      for (; !isdigit(line[last]); --last);

      uint8_t c1 = line[first] - '0', c2 = line[last] - '0';
      sum += (c1 * 10 + c2);
   }

   return sum;
}

size_t task_2(const std::string_view file) {
   static const std::vector<std::string> vec = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

   uint64_t sum = 0;
   for (const std::ranges::subrange<const char*> l : std::views::split(file, '\n')) {
      if (l.size() == 0) break;
      const std::string line {l.begin(), l.end()};
      size_t first = line.find_first_of("0123456789");
      size_t last = line.find_last_of("0123456789");

      size_t first_ind = (first == std::string::npos) ? 100000 : first;
      size_t first_nb = (first == std::string::npos) ? 100000 : (line[first] - '0');
      size_t last_ind = (last == std::string::npos) ? 0 : last;
      size_t last_nb = (last == std::string::npos) ? 0 : (line[last] - '0');
      for (uint i = 0; i < vec.size(); i++) {
         size_t ind_found = line.find(vec[i]);
         if (ind_found != std::string::npos) {
            if (ind_found <= first_ind) {
               first_ind = ind_found;
               first_nb = i + 1;
            }
         }

         ind_found = line.rfind(vec[i]);
         if (ind_found != std::string::npos) {
            if (ind_found >= last_ind) {
               last_ind = ind_found;
               last_nb = i + 1;
            }
         }
      }

      sum += first_nb * 10 + last_nb;
   }

   return sum;
}
