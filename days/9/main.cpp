#include "aoc.hpp"

#include <fstream>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


size_t task_1(std::stringstream file) {
   std::string line;
   size_t sum = 0;
   for (uint i = 0; std::getline(file, line); i++) {
      std::vector<int32_t> line_nbs{};
      size_t ind = 0;
      while (ind < line.size()) {
         int64_t nb = 0;
         bool is_neg = false;
         for (; ind < line.size() && !isdigit(line[ind]); ++ind) {
            if (line[ind] == '-') is_neg = true;
         }
         for (; ind < line.size() && isdigit(line[ind]); ++ind)
            nb = nb * 10 + line[ind] - '0';
         line_nbs.push_back(is_neg ? -nb : nb);
      }

      //We have the list of numbers

      const auto is_all_zeros = [](const auto& in) -> bool {
         return !std::any_of(in.cbegin(), in.cend(), [](const auto e){ return e != 0; });
      };

      std::vector<std::vector<int32_t>> diff_vecs{line_nbs};
      while (!is_all_zeros(diff_vecs.back())) {
         std::vector<int32_t> cur_diff{};
         for (uint j = 1; j < diff_vecs.back().size(); j++) {
            cur_diff.push_back(diff_vecs.back()[j] - diff_vecs.back()[j-1]);
         }

         diff_vecs.push_back(cur_diff);
      }

      //Have all diffs
      for (uint j = 0; j < diff_vecs.size() - 1; j++) {
         sum += diff_vecs[j].back();
      }
   }

   return sum;
}

size_t task_2(std::stringstream file) {
   std::string line;
   size_t sum = 0;
   for (uint i = 0; std::getline(file, line); i++) {
      std::vector<int32_t> line_nbs{};
      size_t ind = 0;
      while (ind < line.size()) {
         int64_t nb = 0;
         bool is_neg = false;
         for (; ind < line.size() && !isdigit(line[ind]); ++ind) {
            if (line[ind] == '-') is_neg = true;
         }
         for (; ind < line.size() && isdigit(line[ind]); ++ind)
            nb = nb * 10 + line[ind] - '0';
         line_nbs.push_back(is_neg ? -nb : nb);
      }

      //We have the list of numbers

      const auto is_all_zeros = [](const auto& in) -> bool {
         return !std::any_of(in.cbegin(), in.cend(), [](const auto e){ return e != 0; });
      };

      std::vector<std::vector<int32_t>> diff_vecs{line_nbs};
      while (!is_all_zeros(diff_vecs.back())) {
         std::vector<int32_t> cur_diff{};
         for (uint j = 1; j < diff_vecs.back().size(); j++) {
            cur_diff.push_back(diff_vecs.back()[j] - diff_vecs.back()[j-1]);
         }

         diff_vecs.push_back(cur_diff);
      }

      std::vector<int32_t> begin_diffs{};
      for (uint k = 0; k < diff_vecs.size(); k++) begin_diffs.push_back(0);
      //Have all diffs
      for (int j = diff_vecs.size() - 2; j >= 0; j--) {
         begin_diffs.at(j) = diff_vecs[j].front() - begin_diffs.at(j+1);
      }
      /* int64_t tmp = diff_vecs.front().front(); */
      /* for (uint j = 1; j < diff_vecs.size() - 1; j++) { */
      /*    std::cout << "tmp: " << tmp << "\n"; */
      /*    tmp -= diff_vecs[j].front(); */
      /* } */

      /* std::cout << "tmp: " << tmp << "\n\n"; */

      /* sum += tmp; */
      /* std::cout << "tmp: " << begin_diffs.front() << "\n\n"; */

      sum += begin_diffs.front();
   }

   return sum;
}
