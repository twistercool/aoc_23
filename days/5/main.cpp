#include "aoc.hpp"

#include <fstream>
#include <ranges>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   size_t task_1_ret = task_1(std::stringstream{file});
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

//
class RangeTranslator {
public:
   int64_t dst;
   int64_t src;
   int64_t len;

   explicit RangeTranslator(int64_t dst, int64_t src, int64_t len):
      dst(dst),
      src(src),
      len(len) {}

   std::vector<RangeTranslator> SimplifyRanges(const RangeTranslator& in) const {
      std::vector<RangeTranslator> ret;

      int64_t start_a = src, end_a = src + len;
      int64_t start_b = src, end_b = in.src + in.len;

      if (start_a > end_b || start_b > end_a) { //No overlapping
         ret.push_back(RangeTranslator(dst, src, len));
         ret.push_back(in);
      } //a is fully in b
      else if (start_a >= start_b && end_a <= end_b) {
         ret.push_back(RangeTranslator(in.dst, in.src, start_a - start_b));
         ret.push_back(RangeTranslator(dst + MapTo() + in.MapTo(), start_a, len));
         ret.push_back(RangeTranslator(in.dst, end_b, end_b - end_a));
      } //b is fully in a
      else if (start_b >= start_a && end_b <= end_a) {
         ret.push_back(RangeTranslator(dst, src, start_b - start_a));
         ret.push_back(RangeTranslator(dst + MapTo() + in.MapTo(), start_b, in.len));
         ret.push_back(RangeTranslator(dst, end_a, end_a - end_b));
      } //b overlaps on the small nbs
      else if (start_a <= start_b && end_a >= start_b) {
         ret.push_back(RangeTranslator(dst, src, start_b - start_a));
         ret.push_back(RangeTranslator(dst + MapTo() + in.MapTo(), start_b, end_a - start_b));
         ret.push_back(RangeTranslator(in.dst, end_a, end_b - end_a));
      } //b overlaps on the largs nbs
      else {
         ret.push_back(RangeTranslator(dst, in.src, start_a - start_b));
         ret.push_back(RangeTranslator(dst + MapTo() + in.MapTo(), start_a, end_b - start_a));
         ret.push_back(RangeTranslator(dst, end_b, end_a - end_b));
      }

      return ret;
   }

   inline bool CanMap(int64_t in) const noexcept {
      return in >= src && in < src + len;
   }

   inline bool CanReverseMap(int64_t in) const noexcept {
      return in >= dst && in < dst + len;
   }

   //Assumes CanMap == true
   //0 is to get the offset
   inline int64_t MapTo(int64_t in = 0) const noexcept {
      return in + dst - src;
   }

   inline int64_t ReverseMapTo(int64_t in = 0) const noexcept {
      return in - dst + src;
   }

   static inline std::optional<RangeTranslator> FromStr(const std::string& line) noexcept {
      size_t ind = 0;
      size_t a = 0, b = 0, c = 0;
      if (line.size() == 0)
         return {};
      //Parse three numbers
      for (; isdigit(line[ind]) && ind < line.size(); ++ind)
         a = a * 10 + line[ind] - '0';
      for (; !isdigit(line[ind]) && ind < line.size(); ++ind);
      for (; isdigit(line[ind]) && ind < line.size(); ++ind)
         b = b * 10 + line[ind] - '0';
      for (; !isdigit(line[ind]) && ind < line.size(); ++ind);
      for (; isdigit(line[ind]) && ind < line.size(); ++ind)
         c = c * 10 + line[ind] - '0';

      return RangeTranslator(a, b, c);
   }

   friend std::ostream& operator<<(std::ostream& os, const RangeTranslator& in) {
      return os << '[' << in.src << '-' << in.src + in.len << "] -> [" << in.dst << '-' << in.dst + in.len << "]";
   }
};


size_t task_1(std::stringstream file) {
   std::string line;
   if (!std::getline(file, line)) { die("Incorrect input"); };

   //Parse first line
   size_t ind = 0;
   for (; !isdigit(line[ind]) && ind < line.size(); ++ind);

   std::vector<size_t> seeds{};
   while (ind < line.size()) {
      size_t nb = 0;
      for (; isdigit(line[ind]) && ind < line.size(); ++ind)
         nb = nb * 10 + line[ind] - '0';
      for (; !isdigit(line[ind]) && ind < line.size(); ++ind) {}
      seeds.push_back(nb);
   }

   if (!std::getline(file, line)) { die("Incorrect input"); };

   std::vector<std::vector<RangeTranslator>> maps = {{}, {}, {}, {}, {}, {}, {}};

   for (uint i = 0; i < maps.size(); i++) {
      if (!std::getline(file, line) || line.find("map") == std::string::npos) die("Incorrect input, expected 'x-to-y' map declaration");

      while (std::getline(file, line) && line.size() > 0) {
         std::optional<RangeTranslator> rt = RangeTranslator::FromStr(line);
         if (!rt) { die("Failed to parse rt"); }
         maps[i].push_back(*rt);
      }
      if (maps[i].size() == 0) { die("No parsed rts"); }
   }

   size_t min_seed = ~size_t{0};
   for (size_t seed: seeds) {
      for (const std::vector<RangeTranslator>& map: maps) {
         for (const RangeTranslator& rt: map) {
            if (rt.CanMap(seed)) {
               seed = rt.MapTo(seed);
               break;
            }
         }
      }
      //Ends with "seed" as the value
      if (min_seed > seed) {
         min_seed = seed;
      }
   }

   return min_seed;
}

size_t task_2(std::stringstream file) {
   std::string line;
   if (!std::getline(file, line)) { die("Incorrect input"); };

   //Parse first line
   size_t ind = 0;
   for (; !isdigit(line[ind]) && ind < line.size(); ++ind);

   std::vector<RangeTranslator> seeds{};
   while (ind < line.size()) {
      size_t nb = 0, len = 0;
      for (; isdigit(line[ind]) && ind < line.size(); ++ind)
         nb = nb * 10 + line[ind] - '0';
      for (; !isdigit(line[ind]) && ind < line.size(); ++ind) {}

      if (ind == line.size()) { die("Seed numbers not in pairs"); }

      for (; isdigit(line[ind]) && ind < line.size(); ++ind)
         len = len * 10 + line[ind] - '0';
      for (; !isdigit(line[ind]) && ind < line.size(); ++ind) {}

      seeds.push_back(RangeTranslator(nb, nb, len));
   }

   if (!std::getline(file, line)) { die("Incorrect input"); };

   std::vector<std::vector<RangeTranslator>> maps = {{}, {}, {}, {}, {}, {}, {}};

   for (uint i = 0; i < maps.size(); i++) {
      if (!std::getline(file, line) || line.find("map") == std::string::npos) die("Incorrect input, expected 'x-to-y' map declaration");

      while (std::getline(file, line) && line.size() > 0) {
         std::optional<RangeTranslator> rt = RangeTranslator::FromStr(line);
         if (!rt) { die("Failed to parse rt"); }
         maps[i].push_back(*rt);
      }
      if (maps[i].size() == 0) { die("No parsed rts"); }
   }

   const auto can_number_be_reverse_mapped = [&](int64_t in) {
      for (const std::vector<RangeTranslator>& map: maps | std::views::reverse) {
         bool can_layer_map = true;
         for (const RangeTranslator& rt: map) {
            if (rt.CanReverseMap(in)) {
               in = rt.ReverseMapTo(in);
               break;
            }
         }
      }

      for (auto rt: seeds) {
         if (rt.CanReverseMap(in)) {
            return true;
         }
      }
      return false;
   };


   for (int64_t a = 0; a < 1'000'000'000'000; a++) {
      if (can_number_be_reverse_mapped(a)) {
         return a;
      }
   }

   return 0;
}
