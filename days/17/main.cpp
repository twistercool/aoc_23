#include "aoc.hpp"

#include <fstream>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <queue>
#include <algorithm>

size_t task_1(std::stringstream file);
size_t task_2(std::stringstream file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());

   Timer t;
   /* size_t task_1_ret = task_1(std::stringstream{file}); */
   size_t task_2_ret = task_2(std::stringstream{file});
   size_t time = t.TimeMicros();

   std::cout 
   /* << "Task 1: " << task_1_ret << "\n" */ 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

class CoordState {
public:
   Coordi32 co;
   Diri32 from;
   int8_t from_nb;

   explicit CoordState(Coordi32 co = {0, 0}, Diri32 from = {0, 0}, uint8_t from_nb = 0) : co(co), from(from), from_nb(from_nb) {}

   bool operator==(const CoordState& a) const = default;
   friend int operator<=>(const CoordState& a, const CoordState& b) {
      if (a.co != b.co) {
         return a.co <=> b.co;
      } else if (a.from != b.from) {
         return a.from <=> b.from;
      } else {
         return a.from_nb - b.from_nb;
      }
   }

   friend std::ostream& operator<<(std::ostream& os, const CoordState& in) {
      os << "co: " << in.co << ", from: " << in.from << ", from_nb: " << in.from_nb;
      //I have absolutely no idea why ostreams don't print in.from_nb correctly, C++ is such a mess
      printf("%u", in.from_nb);

      return os;
   }
};


namespace std {
template<>
struct std::hash<CoordState> {
   size_t operator()(const CoordState& in) const {
      return std::hash<Coordi32>{}(in.co) ^ (std::hash<Diri32>{}(in.from) << in.from_nb);
   }
};
}

size_t task_1(std::stringstream file) {
   std::string line;

   std::vector<std::vector<uint8_t>> map;
   std::map<CoordState, int64_t> distance;
   std::unordered_set<CoordState> visited;

   /* std::map<CoordState, CoordState> previous_debug; */

   for (uint i = 0; std::getline(file, line); i++) {
      map.emplace_back(std::vector<uint8_t>{});
      for (uint j = 0; j < line.size(); j++) {
         map.back().push_back(line[j] - '0');
      }
   }

   const Coordi32 start{0, 0};
   const Coordi32 end(map.at(0).size()-1, map.size()-1);
   distance[ CoordState{start, {0, 0}, 0} ] = 0;

   const auto get_unvisited_neighbours = [&](const CoordState& cur) -> std::vector<CoordState> {
      std::vector<CoordState> ret{};

      const auto check_neighbours_dir = [&](const Diri32 dir) -> void {
         const Coordi32 neigh = cur.co + dir;
         const Diri32 rev_dir = Diri32{-dir.first, -dir.second};

         if (cur.from != rev_dir) {
            CoordState neigh_state {neigh, rev_dir, 1};
            if (!visited.contains(neigh_state)) {
               ret.push_back(neigh_state);
            }
            /* neigh_state.from_nb++; */
            /* if (!visited.contains(neigh_state)) { */
            /*    ret.push_back(neigh_state); */
            /* } */
            /* neigh_state.from_nb++; */
            /* if (!visited.contains(neigh_state)) { */
            /*    ret.push_back(neigh_state); */
            /* } */
         } else {
            if (cur.from_nb < 3) {
               const CoordState neigh_state {neigh, rev_dir, uint8_t(cur.from_nb + 1)};
               if (!visited.contains(neigh_state)) {
                  ret.push_back(neigh_state);
               }
            }
         }
      };

      if (cur.co.second > 0 && cur.from != Diri32{0, -1}) check_neighbours_dir(Diri32{0, -1});
      if (cur.co.second < int(map.size()-1) && cur.from != Diri32{0, 1}) check_neighbours_dir(Diri32{0, 1});
      if (cur.co.first > 0 && cur.from != Diri32{-1, 0}) check_neighbours_dir(Diri32{-1, 0});
      if (cur.co.first < int(map.at(0).size()-1) && cur.from != Diri32{1, 0}) check_neighbours_dir(Diri32{1, 0});

      return ret;
   };

   while (1) {
      //Find next vertex with lowest distance that is also not visited
      CoordState cur{};
      int64_t min_dist = INT64_MAX;
      //Slow but oh well, can do a heap implementation later
      for (const auto& [ coord_state, dist ] : distance) {
         if (!visited.contains(coord_state)) {
            if (dist < min_dist) {
               min_dist = dist;
               cur = coord_state;
            }
         }
      }
      if (min_dist == INT64_MAX) { break; }
      /* std::cout << "Visiting: " << cur.co.first << ":" << cur.co.second << " dist: " << min_dist << "\n"; */

      visited.insert(cur);

      std::vector<CoordState> neighbours = get_unvisited_neighbours(cur);

      //for every unvisited neighbour of cur, 
      for (const CoordState& neigh : neighbours) {
         const auto in_dist = min_dist + map[neigh.co.second][neigh.co.first];
         if (!distance.contains(neigh) || distance[neigh] > in_dist) {
            distance[neigh] = in_dist;
            /* previous_debug[neigh] = cur; */
         }
      }
   }

   /* CoordState end_coord_state_debug{}; */
   int64_t min_dist = INT64_MAX;
   for (const auto& [ co_st, dist ] : distance) {
      if (co_st.co == end) {
         if (min_dist > dist) {
            min_dist = dist;

            /* end_coord_state_debug = co_st; */
         }
      }
   }
   
   /* CoordState cur_co_st = end_coord_state_debug; */
   /* while (previous_debug.contains(cur_co_st)) { */
   /*    std::cout << cur_co_st << ", dist: " << distance[cur_co_st] << "\n"; */

   /*    cur_co_st = previous_debug[cur_co_st]; */
   /* } */
   /* std::cout << start << "\n"; */


   return min_dist;
}

size_t task_2(std::stringstream file) {
   std::string line;

   std::vector<std::vector<uint8_t>> map;
   std::map<CoordState, int64_t> distance;
   std::unordered_set<CoordState> visited;

   /* std::map<CoordState, CoordState> previous_debug; */

   for (uint i = 0; std::getline(file, line); i++) {
      map.emplace_back(std::vector<uint8_t>{});
      for (uint j = 0; j < line.size(); j++) {
         map.back().push_back(line[j] - '0');
      }
   }

   const Coordi32 start{0, 0};
   const Coordi32 end(map.at(0).size()-1, map.size()-1);
   distance[ CoordState{start, {0, 0}, 0} ] = 0;

   const auto get_unvisited_neighbours = [&](const CoordState& cur) -> std::vector<CoordState> {
      std::vector<CoordState> ret{};

      const auto check_neighbours_dir = [&](const Diri32 dir) -> void {
         const Coordi32 neigh = cur.co + dir;
         const Diri32 rev_dir = Diri32{-dir.first, -dir.second};

         if (cur.from != rev_dir) {
            CoordState neigh_state {neigh, rev_dir, 1};
            if ((cur.from_nb == 0 || cur.from_nb >= 4) && !visited.contains(neigh_state)) {
               ret.push_back(neigh_state);
            }
         } else {
            if (cur.from_nb < 10) {
               const CoordState neigh_state {neigh, rev_dir, uint8_t(cur.from_nb + 1)};
               if (!visited.contains(neigh_state)) {
                  ret.push_back(neigh_state);
               }
            }
         }
      };

      if (cur.co.second > 0 && cur.from != Diri32{0, -1}) check_neighbours_dir(Diri32{0, -1});
      if (cur.co.second < int(map.size()-1) && cur.from != Diri32{0, 1}) check_neighbours_dir(Diri32{0, 1});
      if (cur.co.first > 0 && cur.from != Diri32{-1, 0}) check_neighbours_dir(Diri32{-1, 0});
      if (cur.co.first < int(map.at(0).size()-1) && cur.from != Diri32{1, 0}) check_neighbours_dir(Diri32{1, 0});

      return ret;
   };

   //TODO: things to speed up:
   //    1) use a heap/priority queue for distances instead of iterating over all elements of a map (super slow)
   //    2) have a "unvisisted" list/map instead of a visited one
   //    3) Maybe use unordered_map instead of map?

   while (1) {
      //Find next vertex with lowest distance that is also not visited
      CoordState cur{};
      int64_t min_dist = INT64_MAX;
      //Slow but oh well, can do a heap implementation later
      for (const auto& [ coord_state, dist ] : distance) {
         if (!visited.contains(coord_state)) {
            if (dist < min_dist) {
               min_dist = dist;
               cur = coord_state;
            }
         }
      }
      if (min_dist == INT64_MAX) { break; }
      /* std::cout << "Visiting: " << cur.co.first << ":" << cur.co.second << " dist: " << min_dist << "\n"; */

      visited.insert(cur);

      std::vector<CoordState> neighbours = get_unvisited_neighbours(cur);

      //for every unvisited neighbour of cur, 
      for (const CoordState& neigh : neighbours) {
         const auto in_dist = min_dist + map[neigh.co.second][neigh.co.first];
         if (!distance.contains(neigh) || distance[neigh] > in_dist) {
            distance[neigh] = in_dist;
            /* previous_debug[neigh] = cur; */
         }
      }
   }

   /* CoordState end_coord_state_debug{}; */
   int64_t min_dist = INT64_MAX;
   for (const auto& [ co_st, dist ] : distance) {
      if (co_st.co == end) {
         if (min_dist > dist) {
            min_dist = dist;

            /* end_coord_state_debug = co_st; */
         }
      }
   }
   
   /* CoordState cur_co_st = end_coord_state_debug; */
   /* while (previous_debug.contains(cur_co_st)) { */
   /*    std::cout << cur_co_st << ", dist: " << distance[cur_co_st] << "\n"; */

   /*    cur_co_st = previous_debug[cur_co_st]; */
   /* } */
   /* std::cout << start << "\n"; */


   return min_dist;
}
