#include "aoc.hpp"

#include <fstream>
#include <cmath>
#include <string_view>

size_t task_1(const std::string_view file);
size_t task_2(const std::string_view file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   const std::string file(ss.str());
   const std::string_view sv{file};

   Timer t;
   size_t task_1_ret = task_1(sv);
   size_t task_2_ret = task_2(sv);
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}

inline bool double_is_integer(double in) {
   double d = in - std::nearbyint(in);
   return d > -0.001 && d < 0.001;
}

inline size_t task_1(const std::string_view file) {
   std::vector<size_t> a;
   std::vector<size_t> b;
   a.reserve(4);
   b.reserve(4);

   if (file.empty()) { die("Incorrect Input: Empty"); }
   size_t ind = 0;

   while (ind < file.size() && file[ind] != '\n') {
      size_t nb = 0;
      for (; ind < file.size() && !isdigit(file[ind]); ind++) {}
      if (ind == file.size()) { die("Incorrect input: no numbers in first file");}
      for (; ind < file.size() && isdigit(file[ind]); ind++) nb = nb * 10 + file[ind] - '0';
      a.push_back(nb);
   }

   if (file.size() == ind) { die("Incorrect Input: No second line"); }
   ind++;

   while (ind < file.size()) {
      size_t nb = 0;
      for (; ind < file.size() && !isdigit(file[ind]); ind++) {}
      if (ind == file.size()) { break; }
      for (; ind < file.size() && isdigit(file[ind]); ind++) nb = nb * 10 + file[ind] - '0';
      b.push_back(nb);
   }

   if (a.size() != b.size()) { die("Incorrect Input: different amount of numbers per line"); }

   size_t ret = 1;
   for (uint i = 0; i < a.size(); i++) {
      double k_ = a[i], m_ = b[i];
      //assumes a positive delta
      double delta = std::sqrt(k_ * k_ - 4 * m_);
      double x1 = (k_ + delta) / 2.0;
      double x2 = (k_ - delta) / 2.0;
      if (!double_is_integer(x1))
         x1 = std::floor(x1);
      else
         x1--;
      if (!double_is_integer(x2))
         x2 = std::ceil(x2);
      else
         x2++;
      size_t nb = x1 - x2 + 1;
      ret *= nb;
   }

   return ret;
}

inline size_t task_2(const std::string_view file) {
   size_t k = 0;
   size_t m = 0;

   size_t ind = 0;

   while (ind < file.size() && file[ind] != '\n') {
      if (isdigit(file[ind]))
         k = k * 10 + file[ind] - '0';
      ind++;
   }

   if (file.size() == ind || file[ind] != '\n') { die("Incorrect Input: No second line"); }
   ind++;

   while (ind < file.size()) {
      if (isdigit(file[ind]))
         m = m * 10 + file[ind] - '0';
      ind++;
   }

   //assumes a positive delta
   double delta = std::sqrt(k * k - 4 * m);
   double x1 = (k + delta) / 2.0;
   double x2 = (k - delta) / 2.0;
   if (!double_is_integer(x1))
      x1 = std::floor(x1);
   else
      x1--;
   if (!double_is_integer(x2))
      x2 = std::ceil(x2);
   else
      x2++;
   return  x1 - x2 + 1;
}
