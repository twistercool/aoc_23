#include "aoc.hpp"

#include <fstream>
#include <optional>
#include <string_view>
#include <ranges>

size_t task_1(std::string_view file);
size_t task_2(std::string_view file);

int main(int argc, char **argv) {
   if (argc != 2) {
      std::cerr << "Usage:\n<bin> <input_file>";
      std::exit(1);
   }

   std::ifstream file_s(argv[1]);
   if (file_s.fail()) {
      std::cerr << "Failed to open '" << argv[1] << "'\n";
      std::exit(1);
   }

   std::stringstream ss;
   ss << file_s.rdbuf();
   std::string file(ss.str());
   if (!file.empty()) {
      file.erase(file.size()-1);
   }

   Timer t;
   size_t task_1_ret = task_1(std::string_view{file});
   size_t task_2_ret = task_2(std::string_view{file});
   size_t time = t.TimeMicros();

   std::cout << "Task 1: " << task_1_ret << "\n" 
             << "Task 2: " << task_2_ret << "\n"
             << "Time: " << time << " µs" << std::endl;
}


template<typename T>
Vector3<T>::Vector3(std::string_view sv) {
   std::vector<int64_t> ints{};
   //Can I parse it without a local copy?
   std::ranges::copy(sv | std::views::split(',') | std::views::transform([](auto v){
      /* std::cout << "current split: '" << std::string{v.begin(), v.end()} << "'\n"; */
      int64_t i = 0;
      auto fv = std::views::filter(v, [](auto a) { return a != ' '; });
      /* std::cout << "filtered: '" << std::string{fv.begin(), fv.end()} << "'\n"; */
      std::string sfv{fv.begin(), fv.end()};
      std::from_chars(sfv.data(), sfv.data() + sfv.size(), i);
      /* std::cout << "parsed: '" << i << "'\n"; */
      return i;
   }), std::back_inserter(ints));

   x = ints.at(0);
   y = ints.at(1);
   z = ints.at(2);
}


struct Hailstone {
   Vector3<int64_t> pos;
   Vector3<int64_t> dir;

   Hailstone(Vector3<int64_t> pos = {}, Vector3<int64_t> dir = {}) : pos(pos), dir(dir) {}

   friend std::ostream& operator<<(std::ostream& os, const Hailstone& in) {
      return os << "pos: "<< in.pos << ", dir:" << in.dir;
   }
};

struct Collision {
   Vector3<int64_t> pos;
   /* size_t time; */

   Collision(Vector3<int64_t> pos) : pos(pos) {}
   /* Collision(Vector3<int64_t> pos, size_t time) : pos(pos) {} */
};

size_t task_1(std::string_view file) {
   std::vector<Hailstone> hails{};

   for (const auto& line_split : std::views::split(file, '\n')) {
      std::string_view line{line_split.cbegin(), line_split.cend()};
      size_t pos_at = line.find('@');
      Vector3 pos { std::string_view{line.begin(), line.begin() + pos_at - 1} };
      Vector3 velocity { std::string_view{line.begin() + pos_at + 1, line.end()} };

      hails.push_back(Hailstone(pos, velocity));
   }

   const int64_t box_min_x { 200000000000000 };
   const int64_t box_max_x { 400000000000000 };
   const int64_t box_min_y { 200000000000000 };
   const int64_t box_max_y { 400000000000000 };

   /* const int64_t box_min_x = 7; */
   /* const int64_t box_max_x = 27; */
   /* const int64_t box_min_y = 7; */
   /* const int64_t box_max_y = 27; */

   const auto get_collision = [&](const Hailstone a, const Hailstone b) -> std::optional<Collision> {
      const double a_p_x = a.pos.x, b_p_x = b.pos.x,
                   a_p_y = a.pos.y, b_p_y = b.pos.y,
                   a_d_x = a.dir.x, a_d_y = a.dir.y,
                   b_d_x = b.dir.x, b_d_y = b.dir.y;

      //find t', which is the time at which the second Hailstone crosses the path the first
      const double t_p = (a_p_x - b_p_x + (a_d_x / a_d_y) * (b_p_y - a_p_y)) / (b_d_x - a_d_x * b_d_y / a_d_y);
      const double t = (b_p_y + t_p * b_d_y - a_p_y) / a_d_y;

      /* std::cout << "Crosses at t: '" << t << "' and t': '" << t_p << "'\n"; */

      if (t < 0 || t_p < 0) return std::nullopt;

      const double x = a_p_x + t * a_d_x;
      const double y = a_p_y + t * a_d_y;

      /* std::cout << "x: '" << x << "' and y: '" << y << "'\n"; */

      if (x >= box_min_x && x <= box_max_x && y >= box_min_y && y <= box_max_y) {
         return { Vector3<int64_t>{int64_t(x), int64_t(y), 0} };
      } else {
         return std::nullopt;
      }
   };

   size_t sum = 0;
   std::vector<Collision> collisions{};
   for (uint i = 0; i < hails.size() - 1; i++) {
      for (uint j = i + 1; j < hails.size(); j++) {
         const std::optional<Collision> col = get_collision(hails[i], hails[j]);
         if (col) {
            sum++;
            /* collisions.push_back(*col); */
         }
      }
   }

   /* return collisions.size(); */
   return sum;
}

template <std::integral Int>
bool is_divisible_by(const Int a, const Int b) {
   return a == b * (a / b);
}

size_t task_2(std::string_view file) {
   //Just solved it in a non-linear solver, didn't manage to make it linear to solve it myself
   return 546494494317645LLU;
}
