# Adevent of Code 2023 Solutions

These are my solutions to `Advent of Code 2023`, in C++.

If you're a beginner looking for the answers to the questions, make sure that you first had a good, honest try at it.

You can build and run any solution for any day using:
```sh
make run DAY=<insert your day here>
```

You can clean every binary with:
```sh
make clean
```


I tried using a variety of C++ styles to get those answers, so it probably isn't always the absolutely most efficient way of doing something, and that's fine.
