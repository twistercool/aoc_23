VERSION = 4.12.2023
INCS += -Iinclude
LIBS += -lm
CXXFLAGS += -static -std=c++23 ${INCS} -march=native -O3 -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable ${LIBS} -ffast-math
CXX_DEBUG_FLAGS += -DDEBUG -O0 -g
CXX = clang++

SRC = $(wildcard days/*/main.cpp)
BINS = ${SRC:main.cpp=bin}


days/%/bin: days/%/main.cpp
	${CXX} ${CXXFLAGS} $< -o $@
days/%/debug_bin: days/%/main.cpp
	${CXX} ${CXXFLAGS} ${CXX_DEBUG_FLAGS} $< -o $@


ifdef DAY
build: days/${DAY}/bin
debug_build: days/${DAY}/debug_bin
run: build
	@if [ -z "${INPUT}" ]; then \
		if [ -f "days/${DAY}/input.txt" ]; then \
			./days/${DAY}/bin days/${DAY}/input.txt; \
		else \
			echo "Please put your input in 'days/${DAY}/input.txt'"; \
		fi \
	else \
		./days/${DAY}/bin days/${DAY}/${INPUT}; \
	fi

debug_run: debug_build
	if [ -z "${INPUT}" ]; then \
		lldb -- days/${DAY}/debug_bin; \
	else \
		lldb -- days/${DAY}/debug_bin days/${DAY}/${INPUT}; \
	fi

else
build: ${BINS}
	@echo '--BUILD FINISHED--'
run: build
	@for day in {1..25}; do \
		echo "Day $${day}:"; \
		./days/$${day}/bin days/$${day}/input.txt; \
	done
endif

clean:
	@if [ -z "${DAY}" ]; then \
		echo '--CLEANING--'; \
		rm -f days/*/{,debug_}bin; \
	else \
		echo '--CLEANING DAY ${DAY}--'; \
		rm -f days/${DAY}/{,debug_}bin; \
	fi

all: run

.PHONY: all run debug_run clean
