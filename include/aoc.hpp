#pragma once

#include <string>
#include <chrono>
#include <iostream>
#include <vector>
#include <sstream>
#include <queue>
#include <source_location>
/* #include <algorithm> */

/*
   * Bunch of helper classes/inline functions
*/

typedef unsigned int uint;

class Timer {
private:
   const std::string name_;
   std::chrono::time_point<std::chrono::system_clock> start { std::chrono::system_clock::now() };

public:
   Timer(std::string in = "") : name_(in)  {}

   size_t TimeMicros() const {
      const std::chrono::time_point<std::chrono::system_clock> end { std::chrono::system_clock::now() };

      const std::chrono::duration<double, std::micro> elapsed_us = (end - start);
      return static_cast<size_t>(elapsed_us.count());
   }

   size_t TimeMicrosReset() {
      const std::chrono::time_point<std::chrono::system_clock> end { std::chrono::system_clock::now() };

      const std::chrono::duration<double, std::micro> elapsed_us = (end - start);
      start = std::chrono::system_clock::now();
      return static_cast<size_t>(elapsed_us.count());
   }

   size_t TimeNanosReset() {
      const std::chrono::time_point<std::chrono::system_clock> end { std::chrono::system_clock::now() };

      const std::chrono::duration<double, std::nano> elapsed_ns = (end - start);
      start = std::chrono::system_clock::now();
      return static_cast<size_t>(elapsed_ns.count());
   }

   void PrintTimeMicrosReset() {
      if (name_.size() > 0)
         std::cout << "Timer '" << name_ << "': ";
      else
         std::cout << "Time: ";
      std::cout << TimeMicrosReset() << " µs" << std::endl;
   };
};

[[ noreturn ]] inline void die(const std::string print = "", const std::source_location loc = std::source_location::current()) {
   std::cerr << "Died in: "
             << loc.file_name() << "("
             << loc.line() << ":"
             << loc.column() << ") `"
             << loc.function_name() << ((print == "") ? "`\n" : "` with message: ");
   if (print.size() > 0)
      std::cerr << print << std::endl;
   std::terminate();
}

//Useless but cool
struct dier {
   [[ noreturn ]] void operator<<(const std::string in) const {
      die(in);
   }
   [[ noreturn ]] void operator()(const std::string in = "", const std::source_location loc = std::source_location::current()) const {
      die(in, loc);
   }
};
static dier dier;


[[ nodiscard ]] inline auto positive_mod(const auto a, const auto b) {
   return (a % b + b) % b; 
}

using Coordi32 = std::pair<int32_t, int32_t>;
using Diri32 = Coordi32;

inline std::ostream& operator<<(std::ostream& os, const Coordi32& a) {
   /* return os << "(" << a.first << ":" << a.second << ")"; */

   // Neovim index
   return os << "(" << a.second + 1 << ":" << a.first + 1 << ")";
}

//minimum is topmost, left most
//then, anything to the top is smaller
inline int operator<=>(const Coordi32& a, const Coordi32& b) {
   if (a.second != b.second) {
      return a.second - b.second;
   } else {
      return a.first - b.first;
   }
}

inline Coordi32 operator+(const Coordi32& a, const Coordi32& b) {
      return Coordi32{a.first + b.first, a.second + b.second};
}

inline Coordi32 operator-(const Coordi32& a, const Coordi32& b) {
      return Coordi32{a.first - b.first, a.second - b.second};
}

inline Coordi32 operator%(const Coordi32& a, const Coordi32& b) {
      return Coordi32{a.first % b.first, a.second % b.second};
}

namespace std {
template<>
struct std::hash<Coordi32> {
   size_t operator()(const Coordi32& in) const {
      return std::hash<size_t>{}((size_t(in.first) << 32) + size_t(in.second));
   }
};
}

template<typename T = int64_t>
struct Vector3 {
   T x;
   T y;
   T z;

   Vector3(T x =  0, T y = 0, T z = 0) : x(x), y(y), z(z) { }
   Vector3(std::string_view sv);

   friend std::ostream& operator<<(std::ostream& os, const Vector3<T>& in) {
      return os << in.x << ":" << in.y << ":" << in.z;
   }

   friend Vector3<T> operator+(const Vector3<T>& a, const Vector3<T>& b) {
      return Vector3<T>{a.x + b.x, a.y + b.y, a.z + b.z};
   }
   friend Vector3<T> operator-(const Vector3<T>& a, const Vector3<T>& b) {
      return Vector3<T>{a.x - b.x, a.y - b.y, a.z - b.z};
   }

   void operator+=(const Vector3<T>& in) { x += in.x; y += in.y; z += in.z; }
   void operator-=(const Vector3<T>& in) { x -= in.x; y -= in.y; z -= in.z; }
   void operator/=(const auto in) { x /= in; y /= in; z /= in; }
   void operator*=(const auto in) { x *= in; y *= in; z *= in; }

   friend bool operator==(const Vector3<T>& a, const Vector3<T>& b) = default;
};

namespace std {
template<typename T>
struct std::hash<Vector3<T>> {
   size_t operator()(const Vector3<T>& in) const {
      return std::hash<size_t>{}((size_t(in.x)) ^ (~size_t(in.y)) ^ size_t(in.z));
   }
};
}

namespace std {
template<typename T>
ostream& operator<<(ostream& os, const vector<T>& in) {
   os << "vec: [";
   bool is_first = true;
   for (const auto& e : in) {
      os << (is_first ? "" :  " ") << e;
      is_first = false;
   }
   return os << "]\n";
}
template<typename T>
ostream& operator<<(ostream& os, const deque<T>& in) {
   os << "queue: [";
   bool is_first = true;
   for (const auto& e : in) {
      os << (is_first ? "" :  " ") << e;
      is_first = false;
   }
   return os << "]\n";
}
}
